package com.gilang.umjichuktv99.util;

import android.util.Log;

import org.json.JSONObject;

public class CommonFunction {

    private static String CommonTAG = "CommonFunction - ";

    public static String AddHttps(String baseUrl){
        String resultUrl = baseUrl;
        if(!baseUrl.contains("http")) resultUrl = "https:" + resultUrl;

        return resultUrl;
    }

    public static String rndJsonUrl(int fragmentDramaRndNum, String fragmentJson) throws Exception {
        JSONObject jsonObject = new JSONObject(fragmentJson);
        int jsonSize = jsonObject.length();
        Log.d(CommonTAG, "Arr size : " + jsonObject.length());
        String resultUrl = jsonObject.get(""+fragmentDramaRndNum).toString();

        return resultUrl;
    }

    public static boolean isStringDouble(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static String getFirstUrl(String baseUrl){
        String firstUrl = "";
        if(baseUrl != null && !baseUrl.equals("")) {
            String[] urlArr = baseUrl.split("/");
            for (int i = 0; i < 3; i++) {
                firstUrl += urlArr[i];
                if (i != 2) firstUrl += "/";
            }
        }

        return firstUrl;
    }
}
