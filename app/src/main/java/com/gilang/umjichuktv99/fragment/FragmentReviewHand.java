package com.gilang.umjichuktv99.fragment;

import static android.content.Context.MODE_PRIVATE;
import static com.gilang.umjichuktv99.common.CommonMessage.MIN_CLICK_INTERVAL;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gilang.umjichuktv99.R;
import com.gilang.umjichuktv99.activity.list.EpsActivity;
import com.gilang.umjichuktv99.adapter.ListDtamaAdapter;
import com.gilang.umjichuktv99.common.CommonMessage;
import com.gilang.umjichuktv99.item.ListDramaItem;
import com.gomfactory.adpie.sdk.AdView;
import com.gomfactory.adpie.sdk.InterstitialAd;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class FragmentReviewHand extends Fragment implements View.OnClickListener {

    private final String TAG = " FragmentReviewHand - ";
    private ProgressDialog mProgressDialog;

    // 중복 클릭 방지 시간 설정
    private long mLastClickTime;

    private String baseUrl = CommonMessage.baseUrl;
    SharedPreferences pref;
    private GridView gridView;
    GetGridView getGridView;
    private ArrayList<ListDramaItem> listViewItemArr;
    String paramCty = "";
    String banList = "";
    String[] banArr;

    //// paging ////
    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;
    private String lastPage = "1";

    private String intentEpsUrl = "";
    private String intentImgUrl = "";
    private String intentTitle = "";
    private String intentIdNo = "";

    private boolean showInterstitial = false;

    private int adn = 0;
    private int adsCnt = 0;
    private String adsFlag = "";

    private InterstitialAd interstitialAd;
    Handler mHandler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hand, container, false);

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        adsFlag = pref.getString("adsFlag",null);
        paramCty = getArguments().getString("paramCty");

        gridView = (GridView)view.findViewById(R.id.gridview);

        //// paging ////
        tv_currentPage = (TextView)view.findViewById(R.id.fr01_currentpage);
        tv_lastPage = (TextView)view.findViewById(R.id.fr01_lastpage);
        tv_currentPage.setText("1");
        tv_lastPage.setText("1");
        preBtn = (Button)view.findViewById(R.id.fr01_prebtn);
        nextBtn = (Button)view.findViewById(R.id.fr01_nextbtn);
        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        interstitialAd = new InterstitialAd(getActivity(), "613cd10065a17f6519f34f21");
        interstitialAd.setAdListener(
                new com.gomfactory.adpie.sdk.InterstitialAd.InterstitialAdListener() {
                    @Override
                    public void onAdLoaded() {
                        if (interstitialAd.isLoaded()) {
                            //interstitialAd.show();
                        }
                    }
                    @Override
                    public void onAdFailedToLoad(int errorCode) { }
                    @Override
                    public void onAdShown() { }
                    @Override
                    public void onAdClicked() { }
                    @Override
                    public void onAdDismissed() { }
                }
        );
        interstitialAd.load();

        getGridView = new GetGridView();
        getGridView.execute();

        return view;
    }


    public class GetGridView extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<ListDramaItem>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;

            try {
                //Log.d(TAG, "baseUrl : " + "https://www.viki.com/explore?country=korea&language=ko&page=" + pageNum);

                String connUrl = "";
                connUrl = CommonMessage.searchUrl
                        + CommonMessage.paramCty + paramCty + "&"    // 국가
                        + CommonMessage.paramLang + "ko" + "&"   // 자막
                        + CommonMessage.paramPage + pageNum // 페이징
                        + CommonMessage.paramSort;

                Log.d(TAG, "connUrl : " + connUrl);

                doc = Jsoup.connect(connUrl)
                        .header(CommonMessage.headerAccLang, CommonMessage.headerLang)
                        .timeout(20000).get();

                Elements elements = doc.select(".thumbnail");

                for(Element element: elements) {

                    boolean filterFlag = true;

                    String title = element.select("img").attr("alt");
                    String imgUrl = element.select("img").attr("src");
                    String epsUrl = CommonMessage.baseUrl + element.select("a.thumbnail-img").attr("href");
                    String ctn = element.select(".thumb-caption").text();
                    String idNo = element.attr("data-tooltip-id");

                    if(filterFlag) {
                        ListDramaItem listViewItemList = new ListDramaItem(title, imgUrl, epsUrl, ctn, idNo);
                        listViewItemArr.add(listViewItemList);
                    }

                }

                if(lastPage.equals("1")) {
                    Elements pages = doc.select(".pagination .page-link");
                    //Log.d(TAG, "pages : " + pages);
                    if (pages.size() > 2) {
                        lastPage = pages.get(pages.size() - 1).attr("href").split("page=")[1].split("&")[0];
                        //Log.d(TAG, "lastPage : " + lastPage);
                    }
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(getActivity() != null){
                gridView.setAdapter(new ListDtamaAdapter(getActivity(), listViewItemArr, R.layout.item_grid_drama2));

                //// paging ////
                tv_currentPage.setText(pageNum+"");
                tv_lastPage.setText(lastPage);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        long currentClickTime= SystemClock.uptimeMillis();
                        long elapsedTime=currentClickTime-mLastClickTime;
                        mLastClickTime=currentClickTime;

                        // 중복 클릭인 경우
                        if(elapsedTime<=MIN_CLICK_INTERVAL){
                            return;
                        }

                        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));

                        //Log.d(TAG, "adsCnt : " + "   " + adsCnt);
                        //Log.d(TAG, "adsFlag : " + "   " + adsFlag);

                        if(adsCnt == 1 && adsFlag.equals("1")){
                            adsCnt++;
                            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                            editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                            editor.commit(); //완료한다.

                            intentEpsUrl = listViewItemArr.get(position).getEpsUrl();
                            intentImgUrl = listViewItemArr.get(position).getImgUrl();
                            intentTitle = listViewItemArr.get(position).getTitle();
                            intentIdNo = listViewItemArr.get(position).getIdNo();

                            if (interstitialAd.isLoaded()) {
                                interstitialAd.show();
                            } else {
                                Intent intent = new Intent(getActivity(), EpsActivity.class);
                                intent.putExtra("epsUrl", intentEpsUrl);
                                intent.putExtra("imgUrl", intentImgUrl);
                                intent.putExtra("title", intentTitle);
                                intent.putExtra("idNo", intentIdNo);
                                startActivity(intent);
                            }

                        } else {
                            intentEpsUrl = listViewItemArr.get(position).getEpsUrl();
                            intentImgUrl = listViewItemArr.get(position).getImgUrl();
                            intentTitle = listViewItemArr.get(position).getTitle();
                            intentIdNo = listViewItemArr.get(position).getIdNo();

                            Intent intent = new Intent(getActivity(), EpsActivity.class);
                            intent.putExtra("epsUrl", intentEpsUrl);
                            intent.putExtra("imgUrl", intentImgUrl);
                            intent.putExtra("title", intentTitle);
                            intent.putExtra("idNo", intentIdNo);
                            startActivity(intent);
                        }
                    }
                });

            }

            if(mProgressDialog != null) mProgressDialog.dismiss();

        }
    }


    @Override
    public void onClick(View v) {
        long currentClickTime= SystemClock.uptimeMillis();
        long elapsedTime=currentClickTime-mLastClickTime;
        mLastClickTime=currentClickTime;

        // 중복 클릭인 경우
        if(elapsedTime<=MIN_CLICK_INTERVAL){
            return;
        }

        switch (v.getId()){
            case R.id.fr01_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    if(getGridView != null){
                        getGridView.cancel(true);
                    }
                    getGridView = new GetGridView();
                    getGridView.execute();
                }
                break;

            case R.id.fr01_nextbtn :
                if(tv_lastPage.getText() != null && !(tv_lastPage.getText().toString().equals("")) && pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    if(getGridView != null){
                        getGridView.cancel(true);
                    }
                    getGridView = new GetGridView();
                    getGridView.execute();
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getGridView != null){
            getGridView.cancel(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(getGridView != null){
            getGridView.cancel(true);
        }
    }











}
