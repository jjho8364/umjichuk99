package com.gilang.umjichuktv99.fragment;

import static android.content.Context.MODE_PRIVATE;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gilang.umjichuktv99.R;
import com.gilang.umjichuktv99.activity.list.BayListActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class FragmentBayIld extends Fragment implements View.OnClickListener {
    private final String TAG = " FragmentBayIld - ";
    private ProgressDialog mProgressDialog;
    private ListView listView;
    private GetListView getListView = null;
    private String baseUrl = "";
    private String searchUrl = "/tv/search_more/q/1%7C";
    private String type = "";
    private String keyword1 = "";
    private ArrayList<String> pageUrlArr;
    private ArrayList<String> titleArr;

    //// paging ////
    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;
    private String lastPage = "1";

    private EditText editText;
    private Button searchBtn;

    // search
    InputMethodManager inputManager;

    SharedPreferences pref;

    ArrayAdapter<String> adapter;

    private String firstUrl = "";

    private String intentListUrl = "";
    private String intentTitle = "";

    // 중복 클릭 방지 시간 설정
    private static final long MIN_CLICK_INTERVAL=800;
    private long mLastClickTime;

    private boolean showInterstitial = false;


    private int adn = 0;
    private int adsCnt = 0;
    private String adsFlag = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bay_ild, container, false);

        baseUrl = getArguments().getString("baseUrl");
        type = getArguments().getString("type");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));

        listView = (ListView)view.findViewById(R.id.listview);

        adsFlag = pref.getString("adsFlag",null);

        //// paging ////
        tv_currentPage = (TextView)view.findViewById(R.id.fr01_currentpage);
        tv_lastPage = (TextView)view.findViewById(R.id.fr01_lastpage);
        tv_currentPage.setText("1");
        tv_lastPage.setText("1");
        preBtn = (Button)view.findViewById(R.id.fr01_prebtn);
        nextBtn = (Button)view.findViewById(R.id.fr01_nextbtn);
        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        editText = (EditText)view.findViewById(R.id.fr20_edit);
        searchBtn = (Button)view.findViewById(R.id.fr20_searchbtn);
        searchBtn.setOnClickListener(this);

        inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        getListView = new GetListView();
        getListView.execute();

        // get firstUrl
        if(baseUrl != null && !baseUrl.equals("")) {
            String[] urlArr = baseUrl.split("/");
            for (int i = 0; i < 3; i++) {
                firstUrl += urlArr[i];
                if (i != 2) firstUrl += "/";
            }
        }
        //Log.d(TAG, "firstUrl : " + firstUrl);


        return view;
    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        String tempLastPage = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pageUrlArr = new ArrayList<String>();
            titleArr = new ArrayList<String>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            int realPage = pageNum == 1 ? 1 : (pageNum-1)*15;

            try {
                Log.d(TAG, "baseUrl : " + baseUrl + realPage);
                doc = Jsoup.connect(baseUrl + realPage).timeout(15000).get();

                Elements lists = doc.select(".list-group a");

                for(int i=0 ; i<lists.size() ; i++){
                    String title = lists.get(i).text();
                    String pageUrl = lists.get(i).attr("href");

                    titleArr.add(title);
                    pageUrlArr.add(pageUrl);
                }

                ////////////// get lat page /////////////
                Elements lis = doc.select(".pagination");
                if(lis.size() != 0){
                    String[] tempStrArr = lis.select("li").last().select("a").attr("href").split("/");
                    String lastString = tempStrArr[tempStrArr.length-1];

                    if(isStringDouble(lastString)){
                        lastPage = (Integer.parseInt(lastString)/15+1) + "";
                    } else {
                        lastPage = "1";
                    }
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(getActivity() != null){
                //// paging ////
                tv_currentPage.setText(pageNum+"");
                tv_lastPage.setText(lastPage);

                if(titleArr.size() == 0){
                    keyword1 = "";
                    titleArr.clear();
                    adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, titleArr);
                    listView.setAdapter(adapter);
                } else {
                    adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, titleArr);
                    listView.setAdapter(adapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            long currentClickTime= SystemClock.uptimeMillis();
                            long elapsedTime=currentClickTime-mLastClickTime;
                            mLastClickTime=currentClickTime;

                            // 중복 클릭인 경우
                            if(elapsedTime<=MIN_CLICK_INTERVAL){
                                return;
                            }

                            adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));

                            //if(adsCnt == 1 && adsFlag.equals("1")){
                        if(false){
                                adsCnt++;
                                SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                                editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                                editor.commit(); //완료한다.

                                intentListUrl = pageUrlArr.get(position);
                                intentTitle = titleArr.get(position);




                            } else {
                                intentListUrl = pageUrlArr.get(position);
                                intentTitle = titleArr.get(position);

                                Intent intent = new Intent(getActivity(), BayListActivity.class);
                                intent.putExtra("listUrl", intentListUrl);
                                intent.putExtra("firstUrl", firstUrl);
                                intent.putExtra("adsCnt", "0");
                                startActivity(intent);
                            }
                        }
                    });
                }
            }

            if(mProgressDialog != null) mProgressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr01_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    getListView = new GetListView();
                    getListView.execute();
                }
                break;

            case R.id.fr01_nextbtn :
                if(tv_lastPage.getText() != null && !(tv_lastPage.getText().toString().equals("")) && pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    getListView = new GetListView();
                    getListView.execute();
                }
                break;

            case R.id.fr20_searchbtn :
                if(!editText.getText().toString().equals("")){
                    lastPage = "1";
                    keyword1 = editText.getText().toString();
                    baseUrl = firstUrl + searchUrl + keyword1 + type;
                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    pageNum = 1;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    getListView = new GetListView();
                    getListView.execute();

                } else {
                    Toast.makeText(getActivity(), "검색어를 입력하세요.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getListView != null){
            getListView.cancel(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getListView != null){
            getListView.cancel(true);
        }

    }



    public boolean isStringDouble(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }













}