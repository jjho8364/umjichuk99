package com.gilang.umjichuktv99.fragment;

import static android.content.Context.MODE_PRIVATE;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gilang.umjichuktv99.R;
import com.gilang.umjichuktv99.activity.list.HoduListActivity;
import com.gilang.umjichuktv99.adapter.GridDramaAdapter;
import com.gilang.umjichuktv99.item.GridDramaItem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class FragmentHoduDrama extends Fragment implements View.OnClickListener {
    private final String TAG = " FragmentHoduDrama - ";
    private ProgressDialog mProgressDialog;
    private GridView gridView;
    private GetGridView getGridView = null;
    private String baseUrl = "";
    private ArrayList<GridDramaItem> listViewItemArr;
    //// paging ////
    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;
    private String lastPage = "1";

    SharedPreferences pref;

    private String intentListUrl = "";
    private String intentImgUrl = "";
    private String intentTitle = "";

    // 중복 클릭 방지 시간 설정
    private static final long MIN_CLICK_INTERVAL=800;
    private long mLastClickTime;




    private boolean showInterstitial = false;


    private int adn = 0;
    private int adsCnt = 0;
    private String adsFlag = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_drama, container, false);

        baseUrl = getArguments().getString("baseUrl");

        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));






        gridView = (GridView)view.findViewById(R.id.gridview);

        adsFlag = pref.getString("adsFlag",null);

        //// paging ////
        tv_currentPage = (TextView)view.findViewById(R.id.fr01_currentpage);
        tv_lastPage = (TextView)view.findViewById(R.id.fr01_lastpage);
        tv_currentPage.setText("1");
        tv_lastPage.setText("1");
        preBtn = (Button)view.findViewById(R.id.fr01_prebtn);
        nextBtn = (Button)view.findViewById(R.id.fr01_nextbtn);
        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        getGridView = new GetGridView();
        getGridView.execute();

        return view;
    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<GridDramaItem>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            try {
                //Log.d(TAG, "baseUrl : " + baseUrl + pageNum);
                doc = Jsoup.connect(baseUrl + pageNum).timeout(20000).get();

                Elements tempElements = doc.select(".content-new");
                Elements elements = tempElements.get(0).select(".content-new .thumbnail a");

                for(Element element: elements) {

                    String title = element.select("img").attr("alt");
                    String imgUrl = element.select("img").attr("data-src");
                    String listUrl = element.attr("href");
                    String update = element.select(".count_img").text();

                    GridDramaItem gridViewItemList = new GridDramaItem(title, update, imgUrl, listUrl);
                    listViewItemArr.add(gridViewItemList);
                }

                ////////////// get lat page /////////////
                if(lastPage.equals("1")){
                    lastPage = "40";
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(getActivity() != null){
                gridView.setAdapter(new GridDramaAdapter(getActivity(), listViewItemArr, R.layout.item_grid_drama));

                //// paging ////
                tv_currentPage.setText(pageNum+"");
                tv_lastPage.setText(lastPage);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        long currentClickTime= SystemClock.uptimeMillis();
                        long elapsedTime=currentClickTime-mLastClickTime;
                        mLastClickTime=currentClickTime;

                        // 중복 클릭인 경우
                        if(elapsedTime<=MIN_CLICK_INTERVAL){
                            return;
                        }

                        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));

                        //if(adsCnt == 1 && adsFlag.equals("1")){
                        if(false){
                            adsCnt++;
                            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                            editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                            editor.commit(); //완료한다.

                            intentListUrl = listViewItemArr.get(position).getListUrl();
                            intentImgUrl = listViewItemArr.get(position).getImgUrl();
                            intentTitle = listViewItemArr.get(position).getTitle();




                        } else {
                            intentListUrl = listViewItemArr.get(position).getListUrl();
                            intentImgUrl = listViewItemArr.get(position).getImgUrl();
                            intentTitle = listViewItemArr.get(position).getTitle();

                            Intent intent = new Intent(getActivity(), HoduListActivity.class);
                            intent.putExtra("listUrl", intentListUrl);
                            intent.putExtra("title", intentTitle);
                            intent.putExtra("imgUrl", intentImgUrl);
                            intent.putExtra("adsCnt", "0");
                            startActivity(intent);
                        }
                    }
                });
            }

            if(mProgressDialog != null) mProgressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr01_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    if(getGridView != null){
                        getGridView.cancel(true);
                    }
                    getGridView = new GetGridView();
                    getGridView.execute();
                }
                break;

            case R.id.fr01_nextbtn :
                if(tv_lastPage.getText() != null && !(tv_lastPage.getText().toString().equals("")) && pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    if(getGridView != null){
                        getGridView.cancel(true);
                    }
                    getGridView = new GetGridView();
                    getGridView.execute();
                }
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getGridView != null){
            getGridView.cancel(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getGridView != null){
            getGridView.cancel(true);
        }

    }















}
