package com.gilang.umjichuktv99.fragment;

import static android.content.Context.MODE_PRIVATE;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import com.gilang.umjichuktv99.R;
import com.gilang.umjichuktv99.adapter.FakeListViewAdapter;
import com.gilang.umjichuktv99.adapter.GridViewAdapter;
import com.gilang.umjichuktv99.item.FakeDramaItem;
import com.gilang.umjichuktv99.item.GridViewItem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class FakeFragment extends Fragment implements View.OnClickListener {
    private final String TAG = "Fragment - ";
    private ProgressDialog mProgressDialog;
    private GridView gridView;
    private ArrayList<GridViewItem> gridArr;
    private GridViewAdapter adapter;
    private String baseUrl = "https://teddyzaffran.tistory.com/1";

    private ArrayList<String> pageUrlArr;
    private ArrayList<String> titleArr;
    private ArrayList<String> imageArr;

    SharedPreferences pref;
    String clickedUrl = "";

    private int adn = 0;
    private int adsCnt = 0;
    private String adsFlag = "";

    private GetListView getListView = null;

    private ArrayList<FakeDramaItem> listViewItemArr;

    // 중복 클릭 방지 시간 설정
    private static final long MIN_CLICK_INTERVAL=800;
    private long mLastClickTime;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fake_fragment, container, false);

        //baseUrl = getArguments().getString("baseUrl");
        pref= getActivity().getSharedPreferences("pref", MODE_PRIVATE); // 선언
        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));
        gridView = (GridView)view.findViewById(R.id.gridview);

        adsFlag = pref.getString("adsFlag",null);

        adsCnt = 0;

        //getListView = new GetListView();
        //getListView.execute();


        listViewItemArr = new ArrayList<FakeDramaItem>();
        listViewItemArr.add(new FakeDramaItem(
                        "https://i.ytimg.com/vi/e1fmLDPEvAI/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLDeGdLgA21wbuJXI-Qwn8DWQLzmbQ",
                        "https://www.youtube.com/watch?v=e1fmLDPEvAI",
                        "첫날 밤, 신부가 도망갔다?? (우울할 때 보는 중드리뷰)"
                )
        );
        listViewItemArr.add(new FakeDramaItem(
                        "https://i.ytimg.com/vi/ck5jU94HEK8/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLBIiHddrh1BuzU-FfBXJItyInt5hw",
                        "https://www.youtube.com/watch?v=ck5jU94HEK8",
                        "망기니기득애정(forget you, remember love)"
                )
        );
        listViewItemArr.add(new FakeDramaItem(
                        "https://i.ytimg.com/vi/bDS9oEJzaE8/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLBJdxFZ1YFFwvxtju6_V0MIqiNbhw",
                        "https://www.youtube.com/watch?v=bDS9oEJzaE8",
                        "한번쯤은 꼭 봐야 할 인생 중드 [삼생삼세 요약 정리]"
                )
        );
        listViewItemArr.add(new FakeDramaItem(
                        "https://i.ytimg.com/vi/3zyLCCtQczA/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLDIeKDd6r24zhfA5h_ZV1mzELvKMg",
                        "https://www.youtube.com/watch?v=3zyLCCtQczA",
                        "너무 재밌어서 미쳐버리겠는 중드 (핫한 최신중드추천)"
                )
        );
        listViewItemArr.add(new FakeDramaItem(
                        "https://i.ytimg.com/vi/679NR5NLG-M/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAywKoTn5GfKzJt9E9EXR3-SexKlA",
                        "https://www.youtube.com/watch?v=679NR5NLG-M",
                        "일본 드라마 베스트3 !! 2년 만에 돌아온 일본드라마 추천 영상"
                )
        );
        listViewItemArr.add(new FakeDramaItem(
                        "https://i.ytimg.com/vi/_ndD67265zs/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLDyQ2yunhgx4RNIFFn_KON_7_qIhQ",
                        "https://www.youtube.com/watch?v=_ndD67265zs",
                        "세상에서 제일 귀여운 식당 주인 "
                )
        );
        listViewItemArr.add(new FakeDramaItem(
                        "https://i.ytimg.com/vi/p8MW6O9t-s4/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLDcAyHQC25Zw-zN9ALhIvoRrYwcAQ",
                        "https://www.youtube.com/watch?v=p8MW6O9t-s4",
                        "집콕하면서 보기 좋은 대만 드라마 추천 대드 7가지"
                )
        );
        listViewItemArr.add(new FakeDramaItem(
                        "https://i.ytimg.com/vi/SCHBQk1RBrg/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLBjfAMUdiyvrrXyr-xhotIpoJZJbg",
                        "https://www.youtube.com/watch?v=SCHBQk1RBrg",
                        "전교1등 남자가 전교 꼴찌에게 사랑한다고 말하는 독특한 방법"
                )
        );
        listViewItemArr.add(new FakeDramaItem(
                        "https://i.ytimg.com/vi/TQ4CUdl_GCM/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLDxtldZ3WeY_GE6NJCcVKHNTKmx1Q",
                        "https://www.youtube.com/watch?v=TQ4CUdl_GCM",
                        "한번쯤은 꼭 봐야 할 인생 중드 [향밀 요약 정리]"
                )
        );
        listViewItemArr.add(new FakeDramaItem(
                        "https://i.ytimg.com/vi/MIHKjBwqlp8/hqdefault.jpg?sqp=-oaymwEZCPYBEIoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAg5yPH58hgDpUlu8CXi1oyFn6OyQ",
                        "https://www.youtube.com/watch?v=MIHKjBwqlp8",
                        "가슴찢어지는 드라마, 3일내내 오열했던 동궁"
                )
        );

        gridView.setAdapter(new FakeListViewAdapter(getActivity(), listViewItemArr, R.layout.item_grid_drama));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long currentClickTime= SystemClock.uptimeMillis();
                long elapsedTime=currentClickTime-mLastClickTime;
                mLastClickTime=currentClickTime;

                // 중복 클릭인 경우
                if(elapsedTime<=MIN_CLICK_INTERVAL){
                    return;
                }

                adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));

                //if(adsCnt == 1 && adsFlag.equals("1")){
                        if(false){
                    adsCnt++;
                    SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                    editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                    editor.commit(); //완료한다.

                    //

                } else {
                    String videoUrl = listViewItemArr.get(position).getVideoUrl();
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoUrl));
                    startActivity(intent);
                }
            }
        });

        return view;
    }



    public class GetListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pageUrlArr = new ArrayList<String>();
            titleArr = new ArrayList<String>();
            imageArr = new ArrayList<String>();

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            Document doc2 = null;

            try {
                //Log.d(TAG, "baseUrl : " + baseUrl);
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                String strHtml = doc.select("b").text();
                doc2 = Jsoup.parse(strHtml);

                Elements elements = doc2.select(".fakeList");

                //Log.d(TAG, "size : " + elements.size());
                for(Element element: elements) {
                    String title = element.select(".title").text();
                    String imgUrl = element.select(".imgUrl").text();
                    String pageUrl = element.select(".pageUrl").text();

                    titleArr.add(title);
                    pageUrlArr.add(pageUrl);
                    imageArr.add(imgUrl);

                }


            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(getActivity() != null){
                gridArr = new ArrayList<GridViewItem>();

                for(int i=0 ; i<titleArr.size() ; i++){
                    gridArr.add(new GridViewItem(imageArr.get(i),titleArr.get(i),pageUrlArr.get(i)));
                }

                adapter = new GridViewAdapter(getActivity(), gridArr, R.layout.gridviewitem);
                gridView.setAdapter(adapter);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        clickedUrl = gridArr.get(position).getYutubUrl();
                        //Log.d(TAG, "cliked : " + titleArr.get(position) + ", link : " + pageUrlArr.get(position));
                        adsCnt =  Integer.parseInt(pref.getString("adsCnt",null));

                        //if(adsCnt == 1 && adsFlag.equals("1")){
                        if(false){
                            adsCnt++;
                            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
                            editor.putString("adsCnt", "2"); //First라는 key값으로 id 데이터를 저장한다.
                            editor.commit(); //완료한다.

                            //

                        } else {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(clickedUrl));
                            startActivity(intent);
                        }

                    }
                });
            }

            if(mProgressDialog != null) mProgressDialog.dismiss();
        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "fragment On paused");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "fragment onresume");
        //adRequest = new AdRequest.Builder().build();
        //interstitialAd = new InterstitialAd(getActivity());
        //interstitialAd.setAdUnitId(getResources().getString(R.string.full_main));
        //interstitialAd.loadAd(adRequest);

    }

    public String getImgUrl(String fullStr) {
        String str = fullStr;
        String target1 = "(";
        String target2 = ")";
        int taget1Num = str.indexOf(target1);
        int taget2Num = str.indexOf(target2);

        return "https://" + str.substring(taget1Num+1, taget2Num);
    }


}
