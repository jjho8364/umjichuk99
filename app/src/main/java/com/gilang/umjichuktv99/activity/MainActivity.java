package com.gilang.umjichuktv99.activity;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import com.gilang.umjichuktv99.R;
import com.gilang.umjichuktv99.common.CommonMessage;
import com.gilang.umjichuktv99.fragment.FakeFragment;
import com.gilang.umjichuktv99.fragment.FragmentAtkorMid;
import com.gilang.umjichuktv99.fragment.FragmentAtkorMu;
import com.gilang.umjichuktv99.fragment.FragmentBayIld;
import com.gilang.umjichuktv99.fragment.FragmentHoduDrama;
import com.gilang.umjichuktv99.fragment.FragmentKorMid;
import com.gilang.umjichuktv99.fragment.FragmentKorMubi;
import com.gilang.umjichuktv99.fragment.FragmentLinkMid;
import com.gilang.umjichuktv99.fragment.FragmentLive;
import com.gilang.umjichuktv99.fragment.FragmentMaru2;
import com.gilang.umjichuktv99.fragment.FragmentMaruSearch2;
import com.gilang.umjichuktv99.fragment.FragmentReviewHand;
import com.gilang.umjichuktv99.fragment.dramajson.FragmentKoreanz;
import com.gilang.umjichuktv99.fragment.dramajson.FragmentMaruDrama;
import com.gilang.umjichuktv99.fragment.dramajson.FragmentNewdaumDrama;
import com.gilang.umjichuktv99.fragment.dramajson.FragmentSonagi;
import com.gilang.umjichuktv99.fragment.dramajson.FragmentTvusan;
import com.gilang.umjichuktv99.fragment.endcontents.FragmentOtg;
import com.gilang.umjichuktv99.fragment.endcontents.FragmentTvnara;
import com.gilang.umjichuktv99.fragment.joytv.FragmentJoytvDrama;
import com.gilang.umjichuktv99.util.CommonFunction;
import com.gomfactory.adpie.sdk.AdPieSDK;
import com.gomfactory.adpie.sdk.AdView;
import com.gomfactory.adpie.sdk.InterstitialAd;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.Date;

public class MainActivity extends FragmentActivity implements View.OnClickListener {
    private final String TAG = "MainActivityTAG - ";

    private int mCurrentFragmentIndex;
    public final static int FRAGMENT_ONE = 0;
    public final static int FRAGMENT_TWO = 1;
    public final static int FRAGMENT_THREE = 2;
    public final static int FRAGMENT_FOUR = 3;
    public final static int FRAGMENT_FIVE = 4;
    public final static int FRAGMENT_MARU_DRAMA = 26;
    public final static int FRAGMENT_MARU_ENTER = 27;
    public final static int FRAGMENT_BAY_DRAMA = 46;
    public final static int FRAGMENT_BAY_ENTER = 47;
    public final static int FRAGMENT_BAY_MUBI = 29;
    public final static int FRAGMENT_BAY_ILD = 30;
    public final static int FRAGMENT_LINK_DRAMA = 7;
    public final static int FRAGMENT_LINK_ENTER = 8;
    public final static int FRAGMENT_LINK_SISA = 9;
    public final static int FRAGMENT_LINK_MID = 31;
    public final static int FRAGMENT_LINK_JOONGD = 32;
    public final static int FRAGMENT_ATKOR_MUBI_HAN_CLASSIC = 33;
    public final static int FRAGMENT_ATKOR_MUBI_OVER_CLASSIC = 6;
    public final static int FRAGMENT_ATKOR_MID = 34;
    public final static int FRAGMENT_KOR_MUBI_HAN = 35;
    public final static int FRAGMENT_KOR_MUBI_OVER = 36;
    public final static int FRAGMENT_KOR_MID = 37;
    public final static int FRAGMENT_KOR_CD = 38;
    public final static int FRAGMENT_KOR_JD = 39;
    public final static int FRAGMENT_ATKOR_JOONGD = 40;
    public final static int FRAGMENT_ATKOR_ILD = 41;
    public final static int FRAGMENT_LIVE = 42;
    public final static int FRAGMENT_LIVE_ENTER = 43;
    public final static int FRAGMENT_LIVE_SPORTS = 44;
    public final static int FRAGMENT_FAKE_MENU = 45;
    public final static int FRAGMENT_JSON_DRAMA = 46;
    public final static int FRAGMENT_FAKE_ONE = 91;
    public final static int FRAGMENT_FAKE_TWO = 92;
    public final static int FRAGMENT_FAKE_THREE = 93;
    public final static int FRAGMENT_FAKE_FOUR = 94;

    private TextView tv_fragment01;
    private TextView tv_fragment02;
    private TextView tv_fragment03;
    private TextView tv_fragment04;
    private TextView tv_fragment05;
    private TextView tv_fragmentMaruDrama;
    private TextView tv_fragmentMaruEnter;
    private TextView tv_fragmentBayMubi;
    private TextView tv_fragmentLinkDrama;
    private TextView tv_fragmentLinkEnter;
    private TextView tv_fragmentLinkSisa;
    private TextView tv_fragmentAtkorMubiHanClassic;
    private TextView tv_fragmentAtkorMubiOverClassic;
    private TextView tv_fragmentAtkorMid;
    private TextView tv_fragmentKorMubiHan;
    private TextView tv_fragmentKorMubiOver;
    private TextView tv_fragmentKorMid;
    private TextView tv_fragmentKorCd;
    private TextView tv_fragmentKorJd;
    private TextView tv_fragmentAtkorJoongd;
    private TextView tv_fragmentAtkorIld;
    private TextView tv_fragmentFakeOne;
    private TextView tv_fragmentFakeTwo;
    private TextView tv_fragmentFakeThree;
    private TextView tv_fragmentFakeFour;

    private String fragment01Url = "";
    private String fragment02Url = "";
    private String fragment03Url = "";
    private String fragment04Url = "";
    private String fragment05Url = "";
    private String fragmentMaruDrama;
    private String fragmentMaruEnter;
    private String fragmentMaruSearch;
    private String fragmentBayMubi;
    private String fragmentBayIld;
    private String fragmentLinkDrama;
    private String fragmentLinkEnter;
    private String fragmentLinkSisa;
    private String fragmentLinkMid;
    private String fragmentLinkJoongd;
    private String fragmentAtkorMubiHanClassic;
    private String fragmentAtkorMubiOverClassic;
    private String fragmentAtkorMid;
    private String fragmentKorMubiHan;
    private String fragmentKorMubiOver;
    private String fragmentKorMid;
    private String fragmentKorCd;
    private String fragmentKorJd;
    private String fragmentAtkorJoongd;
    private String fragmentAtkorIld;
    private String fragmentLive;
    private String fragmentLiveEnter;
    private String fragmentLiveSports;
    private String fragmentDramaJson;
    private String fragmentEnterJson;
    private String fragmentSisaJson;
    private int fragmentDramaRndNum;
    private int fragmentEnterRndNum;
    private int fragmentSisaRndNum;
    private String fragmentJoytvDrama;
    private String fragmentJoytvEnter;
    private String fragmentJoytvSisa;
    private String fragmentEndContentsOtg;
    private String fragmentEndContentsTvnara;
    private String fragmentEndContents;

    private String nextAppUrl = "";
    private String mxPlayerUrl = "";
    private Date curDate;
    private String appStatus = "99";

    Date d1;
    Date d2;

    Date installedDate;

    // native ads
    private Button refresh;
    //private UnifiedNativeAd nativeAd;

    // fk ads
    private WebView webView;
    private String fkAdsUrl = "";

    private String tistoryUrl = "";
    private String tistoryRndPage = "";

    // alert dialog
    Dialog dialog;
    TextView finishApp;
    TextView finishAppCancel;

    private int adsCnt = 0;
    private String adsFlag = "";
    int searchRnd = 0;

    // adpie
    private AdView adView;
    private InterstitialAd interstitialAd;
    Handler mHandler;

    Fragment newFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        AdPieSDK.getInstance().initialize(getApplicationContext(), "612ee73365a17f6467d5dcb2");

        /////// start close ads
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle("종료 하시겠습니까?");

        TextView tv_finish_app = dialog.findViewById(R.id.tv_finish_app);
        TextView tv_finish_app_cancel = dialog.findViewById(R.id.tv_finish_app_cancel);

        tv_finish_app.setOnClickListener(this);
        tv_finish_app_cancel.setOnClickListener(this);

        ////////////////////////////// end cloase ads

        Intent mainIntent = getIntent();
        appStatus = mainIntent.getStringExtra("appStatus");
        Log.d(TAG, "appStatus : " + appStatus);

        //tistoryUrl = mainIntent.getStringExtra("tistoryUrl");
        tistoryRndPage = mainIntent.getStringExtra("tistoryRndPage");

        d1 = new Date();

        SharedPreferences pref= getSharedPreferences("pref", MODE_PRIVATE); // 선언
        String first = pref.getString("first",null);
        long installedTime = pref.getLong("installedTime",0);
        if(first != null && !appStatus.equals("3") && !appStatus.equals("4") && !appStatus.equals("99"))  appStatus = "1";

        adsFlag = pref.getString("adsFlag",null);
        //Log.d(TAG, "app log : adsFlag : " + adsFlag);

        if(appStatus.equals("99")){
            setContentView(R.layout.activity_main);

            adView = (AdView) findViewById(R.id.ad_view);
            adView.setScaleUp(true);
            adView.setAutoBgColor(true);
            adView.load();

            HorizontalScrollView horview01 = (HorizontalScrollView)findViewById(R.id.horview01);
            HorizontalScrollView horview02 = (HorizontalScrollView)findViewById(R.id.horview02);
            HorizontalScrollView horview03 = (HorizontalScrollView)findViewById(R.id.horview03);
            HorizontalScrollView horview04 = (HorizontalScrollView)findViewById(R.id.horview04);
            horview01.setVisibility(View.GONE);
            horview02.setVisibility(View.GONE);
            horview03.setVisibility(View.GONE);
            horview04.setVisibility(View.GONE);

            tv_fragmentFakeOne = (TextView)findViewById(R.id.tv_fragment91);
            tv_fragmentFakeTwo = (TextView)findViewById(R.id.tv_fragment92);
            tv_fragmentFakeThree = (TextView)findViewById(R.id.tv_fragment93);
            tv_fragmentFakeFour = (TextView)findViewById(R.id.tv_fragment94);

            tv_fragmentFakeOne.setOnClickListener(this);
            tv_fragmentFakeTwo.setOnClickListener(this);
            tv_fragmentFakeThree.setOnClickListener(this);
            tv_fragmentFakeFour.setOnClickListener(this);

            mCurrentFragmentIndex = FRAGMENT_FAKE_ONE;     // 첫 Fragment 를 초기화
            fragmentReplace(mCurrentFragmentIndex);

        } else if(appStatus.equals("1")){
            setContentView(R.layout.activity_main);

            adView = (AdView) findViewById(R.id.ad_view);
            adView.setScaleUp(true);
            adView.setAutoBgColor(true);
            adView.setAdListener(new com.gomfactory.adpie.sdk.AdView.AdListener() {
                @Override
                public void onAdLoaded() { }
                @Override
                public void onAdFailedToLoad(int errorCode) { }
                @Override
                public void onAdClicked() { }
            });
            adView.load();

            interstitialAd = new InterstitialAd(this, "613cd10065a17f6519f34f21");
            interstitialAd.setAdListener(
                    new com.gomfactory.adpie.sdk.InterstitialAd.InterstitialAdListener() {
                        @Override
                        public void onAdLoaded() {
                            if (interstitialAd.isLoaded()) {
                                mHandler = new Handler();
                                mHandler.postDelayed(new Runnable() {
                                    public void run() {
                                        interstitialAd.show();
                                    }
                                }, 4000); // 0.5초후
                            }
                        }
                        @Override
                        public void onAdFailedToLoad(int errorCode) { }
                        @Override
                        public void onAdShown() { }
                        @Override
                        public void onAdClicked() { }
                        @Override
                        public void onAdDismissed() { }
                    }
            );
            interstitialAd.load();

            curDate = new Date();

            HorizontalScrollView horview05 = (HorizontalScrollView)findViewById(R.id.horview05);
            horview05.setVisibility(View.GONE);

            tv_fragment01 = (TextView)findViewById(R.id.tv_fragment01);
            tv_fragment02 = (TextView)findViewById(R.id.tv_fragment02);
            tv_fragment03 = (TextView)findViewById(R.id.tv_fragment03);
            tv_fragment04 = (TextView)findViewById(R.id.tv_fragment04);
            tv_fragment05 = (TextView)findViewById(R.id.tv_fragment05);
            tv_fragmentMaruDrama = (TextView)findViewById(R.id.tv_frag_maru_drama);
            tv_fragmentMaruEnter = (TextView)findViewById(R.id.tv_frag_maru_enter);
            tv_fragmentBayMubi = (TextView)findViewById(R.id.tv_fragment_bay_mubi);
            tv_fragmentLinkDrama = (TextView)findViewById(R.id.tv_fragment_link_drama);
            tv_fragmentLinkEnter = (TextView)findViewById(R.id.tv_fragment_link_enter);
            tv_fragmentLinkSisa = (TextView)findViewById(R.id.tv_fragment_link_sisa);
            tv_fragmentAtkorMubiHanClassic = (TextView)findViewById(R.id.tv_frag_atkor_mubi_han_classic);
            tv_fragmentAtkorMubiOverClassic = (TextView)findViewById(R.id.tv_frag_atkor_mubi_over_classic);
            tv_fragmentAtkorMid = (TextView)findViewById(R.id.tv_frag_atkor_mid);
            tv_fragmentAtkorJoongd = (TextView)findViewById(R.id.tv_frag_atkor_joongd);
            tv_fragmentAtkorIld = (TextView)findViewById(R.id.tv_frag_atkor_ild);
            tv_fragmentKorMubiHan = (TextView)findViewById(R.id.tv_frag_kor_mubi_han);
            tv_fragmentKorMubiOver = (TextView)findViewById(R.id.tv_frag_kor_mubi_over);
            tv_fragmentKorMid = (TextView)findViewById(R.id.tv_frag_kor_mid);
            tv_fragmentKorCd = (TextView)findViewById(R.id.tv_frag_kor_cd);
            tv_fragmentKorJd = (TextView)findViewById(R.id.tv_frag_kor_jd);
            //fragmentFakeMenu = (TextView)findViewById(R.id.tv_fake_menu);

            tv_fragment01.setOnClickListener(this);
            tv_fragment02.setOnClickListener(this);
            tv_fragment03.setOnClickListener(this);
            tv_fragment04.setOnClickListener(this);
            tv_fragment05.setOnClickListener(this);
            tv_fragmentMaruDrama.setOnClickListener(this);
            tv_fragmentMaruEnter.setOnClickListener(this);
            tv_fragmentBayMubi.setOnClickListener(this);
            tv_fragmentLinkDrama.setOnClickListener(this);
            tv_fragmentLinkEnter.setOnClickListener(this);
            tv_fragmentLinkSisa.setOnClickListener(this);
            tv_fragmentAtkorMubiHanClassic.setOnClickListener(this);
            tv_fragmentAtkorMubiOverClassic.setOnClickListener(this);
            tv_fragmentAtkorMid.setOnClickListener(this);
            tv_fragmentAtkorJoongd.setOnClickListener(this);
            tv_fragmentAtkorIld.setOnClickListener(this);
            tv_fragmentKorMubiHan.setOnClickListener(this);
            tv_fragmentKorMubiOver.setOnClickListener(this);
            tv_fragmentKorMid.setOnClickListener(this);
            tv_fragmentKorCd.setOnClickListener(this);
            tv_fragmentKorJd.setOnClickListener(this);

            fragment01Url = mainIntent.getStringExtra("fragment01Url");
            fragment02Url = mainIntent.getStringExtra("fragment02Url");
            fragment03Url = mainIntent.getStringExtra("fragment03Url");
            fragment04Url = mainIntent.getStringExtra("fragment04Url");
            fragment05Url = mainIntent.getStringExtra("fragment05Url");
            //fragmentMaruDrama = mainIntent.getStringExtra("fragMaruDrama2");
            //fragmentMaruEnter = mainIntent.getStringExtra("fragMaruEnter2");
            fragmentMaruDrama = mainIntent.getStringExtra("fragBayDrama");
            fragmentMaruEnter = mainIntent.getStringExtra("fragBayEnter");
            fragmentMaruSearch = mainIntent.getStringExtra("fragMaruSearch");
            fragmentBayMubi = mainIntent.getStringExtra("fragBayMubi");
            fragmentBayIld = mainIntent.getStringExtra("fragBayIld");
            fragmentLinkDrama = mainIntent.getStringExtra("fragLinkDrama");
            fragmentLinkEnter = mainIntent.getStringExtra("fragLinkEnter");
            fragmentLinkSisa = mainIntent.getStringExtra("fragLinkSisa");
            fragmentLinkMid = mainIntent.getStringExtra("fragLinkMid");
            fragmentLinkJoongd = mainIntent.getStringExtra("fragLinkJoongd");
            fragmentAtkorMubiHanClassic = mainIntent.getStringExtra("fragAtkorMubiHanClassic");
            fragmentAtkorMubiOverClassic = mainIntent.getStringExtra("fragAtkorMubiOverClassic");
            fragmentAtkorMid = mainIntent.getStringExtra("fragAtkorMid");
            fragmentAtkorJoongd = mainIntent.getStringExtra("fragAtkorJoongd");
            fragmentAtkorIld = mainIntent.getStringExtra("fragAtkorIld");
            fragmentKorMubiHan = mainIntent.getStringExtra("fragKorMubiHan");
            fragmentKorMubiOver = mainIntent.getStringExtra("fragKorMubiOver");
            fragmentKorMid = mainIntent.getStringExtra("fragKorMid");
            fragmentKorCd = mainIntent.getStringExtra("fragKorCd");
            fragmentKorJd = mainIntent.getStringExtra("fragKorJd");
            fragmentLive = mainIntent.getStringExtra("fragLiveTv");
            fragmentLiveEnter = mainIntent.getStringExtra("fragLiveTvEnter");
            fragmentLiveSports = mainIntent.getStringExtra("fragLiveTvSports");
            fragmentDramaJson = mainIntent.getStringExtra("fragmentDramaJson");
            fragmentEnterJson = mainIntent.getStringExtra("fragmentEnterJson");
            fragmentSisaJson = mainIntent.getStringExtra("fragmentSisaJson");
            fragmentJoytvDrama = mainIntent.getStringExtra("fragmentJoytvDrama");
            fragmentJoytvEnter = mainIntent.getStringExtra("fragmentJoytvEnter");
            fragmentJoytvSisa = mainIntent.getStringExtra("fragmentJoytvSisa");
            fragmentEndContentsOtg = mainIntent.getStringExtra("fragmentEndContentsOtg");
            fragmentEndContentsTvnara = mainIntent.getStringExtra("fragmentEndContentsTvnara");


            try {
                JSONObject jsonObject = new JSONObject(fragmentDramaJson);
                fragmentDramaRndNum = (int)(Math.random() * jsonObject.length());
                fragmentEnterRndNum = (int)(Math.random() * jsonObject.length());
                fragmentSisaRndNum = (int)(Math.random() * jsonObject.length());
                fragmentDramaJson = CommonFunction.rndJsonUrl(fragmentDramaRndNum, fragmentDramaJson);
                fragmentEnterJson = CommonFunction.rndJsonUrl(fragmentEnterRndNum, fragmentEnterJson);
                fragmentSisaJson = CommonFunction.rndJsonUrl(fragmentSisaRndNum, fragmentSisaJson);

                //Log.d(TAG, "fragmentDramaJson : " + fragmentDramaJson);
                //Log.d(TAG, "fragmentEnterJson : " + fragmentEnterJson);
                //Log.d(TAG, "fragmentSisaJson : " + fragmentSisaJson);

            } catch (Exception e) {
                e.printStackTrace();
            }

            searchRnd = 0;//(int)(Math.random() * 2);
            if(searchRnd == 0){
                fragmentEndContents = fragmentEndContentsOtg;
            } else {
                fragmentEndContents = fragmentEndContentsTvnara;
            }

            //mCurrentFragmentIndex = FRAGMENT_MARU_SEARCH;     // 첫 Fragment 를 초기화
            mCurrentFragmentIndex = FRAGMENT_BAY_DRAMA;     // 첫 Fragment 를 초기화
            fragmentReplace(mCurrentFragmentIndex);

            SharedPreferences.Editor editor = pref.edit();// editor에 put 하기
            editor.putString("first", "1"); //First라는 key값으로 id 데이터를 저장한다.
            editor.putString("adsCnt", "1"); //First라는 key값으로 id 데이터를 저장한다.

            if(installedTime == 0 && true){
                installedTime = new Date().getTime();
                //Log.d(TAG, "first install : " + installedTime);
                editor.putLong("installedTime", installedTime); //First라는 key값으로 id 데이터를 저장한다.
            } else {
                long nowTime = new Date().getTime();
                long diff = nowTime - installedTime;
                long min = diff/1000/60 ;
                long hour = min/60;
                //Log.d(TAG, "now - istalled time : " + hour);
                if(hour >= 8) {
                    // fake ads is here
                    //tv_fragmentLive.setVisibility(View.VISIBLE);
                    //tv_fragmentLiveEnter.setVisibility(View.VISIBLE);
                    //tv_fragmentLiveSports.setVisibility(View.VISIBLE);
                }
            }

            editor.commit(); //완료한다.


            int tempRnd = (int)(Math.random() * 10);
            //Log.d(TAG, "tempRnd : " + tempRnd);
            //if(tempRnd == 0) webView.loadUrl(fkAdsUrl);

        } else if(appStatus.equals("2")){
            setContentView(R.layout.maintenance);
            String maintenanceImgUrl = mainIntent.getStringExtra("maintenance");
            ImageView imgView = (ImageView)findViewById(R.id.img_maintenance);
            if(maintenanceImgUrl == null || maintenanceImgUrl.equals("")){
                imgView.setImageResource(R.drawable.noimage);
            } else {
                Picasso.with(this).load(maintenanceImgUrl).into(imgView);
            }
        } else if(appStatus.equals("3")){
            setContentView(R.layout.closed);
            String closedImgUrl = mainIntent.getStringExtra("closed");
            ImageView imgView = (ImageView)findViewById(R.id.img_closed);
            nextAppUrl = mainIntent.getStringExtra("nextAppUrl");
            if(closedImgUrl == null || closedImgUrl.equals("")){
                imgView.setImageResource(R.drawable.noimage);
            } else {
                Picasso.with(this).load(closedImgUrl).into(imgView);
            }
            Button btnClosed = (Button) findViewById(R.id.btn_closed);
            btnClosed.setOnClickListener(this);
        } else if(appStatus.equals("4")) {   // mx player


        } else if(appStatus.equals("9")) {   // mx player
            setContentView(R.layout.closed);
            String closedImgUrl = mainIntent.getStringExtra("closedImgUrl");
            nextAppUrl = mainIntent.getStringExtra("nextAppUrl");
            //Log.d(TAG, "  " + nextAppUrl);
            ImageView imgView = (ImageView)findViewById(R.id.img_closed);
            Picasso.with(this).load(closedImgUrl).into(imgView);
            Button btnClosed = (Button) findViewById(R.id.btn_closed);
            btnClosed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent marketLaunch1 = new Intent(Intent.ACTION_VIEW);
                    marketLaunch1.setData(Uri.parse(nextAppUrl));
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(nextAppUrl));
                    startActivity(intent);
                }
            });
        } else {
            HorizontalScrollView horview01 = (HorizontalScrollView)findViewById(R.id.horview01);
            HorizontalScrollView horview02 = (HorizontalScrollView)findViewById(R.id.horview02);
            HorizontalScrollView horview03 = (HorizontalScrollView)findViewById(R.id.horview03);
            HorizontalScrollView horview04 = (HorizontalScrollView)findViewById(R.id.horview04);
            horview01.setVisibility(View.GONE);
            horview02.setVisibility(View.GONE);
            horview03.setVisibility(View.GONE);


        }

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.tv_fragment01:
                offColorTv();
                tv_fragment01.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ONE;
                fragmentReplace(mCurrentFragmentIndex);
                break;


            case R.id.tv_fragment02:
                offColorTv();
                tv_fragment02.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_TWO;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment03:
                offColorTv();
                tv_fragment03.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_THREE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment04:
                offColorTv();
                tv_fragment04.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FOUR;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment05:
                offColorTv();
                tv_fragment05.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FIVE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            /*case R.id.tv_frag_maru_drama:
                offColorTv();
                tv_fragmentMaruDrama.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_MARU_DRAMA;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_maru_enter:
                offColorTv();
                tv_fragmentMaruEnter.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_MARU_ENTER;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_maru_drama:
                offColorTv();
                tv_fragmentMaruDrama.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_BAY_DRAMA;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_maru_enter:
                offColorTv();
                tv_fragmentMaruEnter.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_BAY_ENTER;
                fragmentReplace(mCurrentFragmentIndex);
                break;
             */
            case R.id.tv_frag_maru_drama:
                offColorTv();
                tv_fragmentMaruDrama.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_BAY_DRAMA;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_maru_enter:
                offColorTv();
                tv_fragmentMaruEnter.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_BAY_ENTER;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment_bay_mubi:
                offColorTv();
                tv_fragmentBayMubi.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_BAY_MUBI;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment_link_drama:
                offColorTv();
                tv_fragmentLinkDrama.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_LINK_DRAMA;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment_link_enter:
                offColorTv();
                tv_fragmentLinkEnter.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_LINK_ENTER;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment_link_sisa:
                offColorTv();
                tv_fragmentLinkSisa.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_LINK_SISA;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_atkor_mubi_han_classic:
                offColorTv();
                tv_fragmentAtkorMubiHanClassic.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ATKOR_MUBI_HAN_CLASSIC;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_atkor_mubi_over_classic:
                offColorTv();
                tv_fragmentAtkorMubiOverClassic.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ATKOR_MUBI_OVER_CLASSIC;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_atkor_mid:
                offColorTv();
                tv_fragmentAtkorMid.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ATKOR_MID;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_atkor_joongd:
                offColorTv();
                tv_fragmentAtkorJoongd.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ATKOR_JOONGD;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_atkor_ild:
                offColorTv();
                tv_fragmentAtkorIld.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_ATKOR_ILD;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_mubi_han:
                offColorTv();
                tv_fragmentKorMubiHan.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_MUBI_HAN;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_mubi_over:
                offColorTv();
                tv_fragmentKorMubiOver.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_MUBI_OVER;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_mid:
                offColorTv();
                tv_fragmentKorMid.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_MID;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_cd:
                offColorTv();
                tv_fragmentKorCd.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_CD;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_frag_kor_jd:
                offColorTv();
                tv_fragmentKorJd.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_KOR_JD;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment91:
                offColorTv2();
                tv_fragmentFakeOne.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FAKE_ONE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment92:
                offColorTv2();
                tv_fragmentFakeTwo.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FAKE_TWO;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment93:
                offColorTv2();
                tv_fragmentFakeThree.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FAKE_THREE;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.tv_fragment94:
                offColorTv2();
                tv_fragmentFakeFour.setBackgroundResource(R.drawable.fragment_selected);
                mCurrentFragmentIndex = FRAGMENT_FAKE_FOUR;
                fragmentReplace(mCurrentFragmentIndex);
                break;
            case R.id.btn_closed:
                Intent marketLaunch1 = new Intent(Intent.ACTION_VIEW);
                Log.d(TAG, "  " + nextAppUrl);
                marketLaunch1.setData(Uri.parse(nextAppUrl));
                startActivity(marketLaunch1);
                break;
            case R.id.tv_finish_app:
                if(dialog != null) dialog.cancel();
                finish();
                break;
            case R.id.tv_finish_app_cancel:
                if(dialog != null) dialog.cancel();
                break;
        }
    }

    private Fragment getFragment(int idx) {
        if(newFragment != null){
            newFragment.onDestroy();
        }

        newFragment = null;
        Bundle args = new Bundle();
        args.putInt("adsCnt", adsCnt);

        switch (idx) {
            case FRAGMENT_ONE:
                if(searchRnd == 0){
                    newFragment = new FragmentOtg();
                } else {
                    newFragment = new FragmentTvnara();
                }
                args.putString("baseUrl", fragmentEndContents); ////////////////////////
                newFragment.setArguments(args);
                break;
            case FRAGMENT_TWO:
                newFragment = new FragmentHoduDrama();
                args.putString("baseUrl", fragment02Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_THREE:
                newFragment = new FragmentHoduDrama();
                args.putString("baseUrl", fragment03Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FOUR:
                newFragment = new FragmentHoduDrama();
                args.putString("baseUrl", fragment04Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FIVE:
                newFragment = new FragmentHoduDrama();
                args.putString("baseUrl", fragment05Url);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_MARU_DRAMA:
                newFragment = new FragmentMaru2();
                args.putString("baseUrl", fragmentMaruDrama);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_MARU_ENTER:
                newFragment = new FragmentMaru2();
                args.putString("baseUrl", fragmentMaruEnter);
                newFragment.setArguments(args);
                break;
                /*
            case FRAGMENT_BAY_DRAMA:
                newFragment = new FragmentBayIld();
                args.putString("baseUrl", fragmentMaruDrama);
                args.putString("type", "%7C0/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_BAY_ENTER:
                newFragment = new FragmentBayIld();
                args.putString("baseUrl", fragmentMaruEnter);
                args.putString("type", "%7C0/page/");
                newFragment.setArguments(args);
                break;
                 */
            case FRAGMENT_BAY_DRAMA:

                if(fragmentDramaJson.contains("sonagitv")){
                    newFragment = new FragmentSonagi();
                } else if(fragmentDramaJson.contains("tvusan")){
                    newFragment = new FragmentTvusan(); //new FragmentTvusan();
                    args.putString("searchUrl", "/bbs/search.php?sfl=wr_subject%7C%7Cwr_content&sop=and&gr_id=vod&srows=10&onetable=tvdrama&stx=");
                } else if(fragmentDramaJson.contains("newsdaum")){
                    newFragment = new FragmentNewdaumDrama();
                    args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                } else if(fragmentDramaJson.contains("koreanz")){
                    newFragment = new FragmentKoreanz();
                    args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                } else if(fragmentDramaJson.contains("marutv")){
                    newFragment = new FragmentMaruDrama();
                    args.putString("searchUrl", "/bbs/board.php?bo_table=tvdrama&sca=&sfl=wr_subject&sop=and&stx=");
                } else {
                    newFragment = new FragmentMaruDrama();
                    args.putString("searchUrl", "/bbs/board.php?bo_table=tvdrama&sca=&sfl=wr_subject&sop=and&stx=");
                }


                /*
                if(fragmentDramaRndNum == 0){
                    newFragment = new FragmentSonagi();
                } else if (fragmentDramaRndNum == 1) {
                    newFragment = new FragmentTvusan(); //new FragmentTvusan();
                    args.putString("searchUrl", "/bbs/search.php?sfl=wr_subject%7C%7Cwr_content&sop=and&gr_id=vod&srows=10&onetable=tvdrama&stx=");
                } else if (fragmentDramaRndNum == 2) {
                    newFragment = new FragmentNewdaumDrama();
                    args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                } else if (fragmentDramaRndNum == 3) {
                    newFragment = new FragmentKoreanz();
                    args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                } else if (fragmentDramaRndNum == 4) {
                    newFragment = new FragmentMaruDrama();
                    args.putString("searchUrl", "/bbs/board.php?bo_table=tvdrama&sca=&sfl=wr_subject&sop=and&stx=");
                }
                */
                args.putString("baseUrl", fragmentDramaJson);
                //args.putString("type", "%7C0/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_BAY_ENTER:
                if(fragmentEnterRndNum == 0){
                    newFragment = new FragmentSonagi();
                } else if (fragmentEnterRndNum == 1) {
                    newFragment = new FragmentTvusan();
                    args.putString("searchUrl", "/bbs/search.php?sfl=wr_subject%7C%7Cwr_content&sop=and&gr_id=vod&srows=10&onetable=tventer&stx=");
                } else if (fragmentEnterRndNum == 2) {
                    newFragment = new FragmentNewdaumDrama();
                    args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                } else if (fragmentEnterRndNum == 3) {
                    newFragment = new FragmentKoreanz();
                    args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                } else if (fragmentDramaRndNum == 4) {
                    newFragment = new FragmentMaruDrama();
                    args.putString("searchUrl", "/bbs/board.php?bo_table=tvyeneng&sca=&sfl=wr_subject&sop=and&stx=");
                }
                args.putString("baseUrl", fragmentEnterJson);
                args.putString("type", "%7C0/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LINK_SISA:
                if(fragmentSisaRndNum == 0){
                    newFragment = new FragmentSonagi();
                } else if (fragmentSisaRndNum == 1) {
                    newFragment = new FragmentTvusan();
                    args.putString("searchUrl", "/bbs/search.php?sfl=wr_subject%7C%7Cwr_content&sop=and&gr_id=vod&srows=10&onetable=tvcult&stx=");
                } else if (fragmentSisaRndNum == 2) {
                    newFragment = new FragmentNewdaumDrama();
                    args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                } else if (fragmentEnterRndNum == 3) {
                    newFragment = new FragmentKoreanz();
                    args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                } else if (fragmentDramaRndNum == 4) {
                    newFragment = new FragmentMaruDrama();
                    args.putString("searchUrl", "/bbs/board.php?bo_table=tvsisa&sca=&sfl=wr_subject&sop=and&stx=");
                }
                args.putString("baseUrl", fragmentSisaJson);
                args.putString("type", "%7C0/page/");
                newFragment.setArguments(args);
                break;
            /*
            case FRAGMENT_BAY_MUBI:
                newFragment = new FragmentBayIld();
                args.putString("baseUrl", fragmentBayMubi);
                args.putString("type", "%7C4/page/");
                newFragment.setArguments(args);
                break;
             */
            case FRAGMENT_BAY_ILD:
                newFragment = new FragmentBayIld();
                args.putString("baseUrl", fragmentBayIld);
                args.putString("type", "%7C7/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LINK_DRAMA:   ///////  joytv drama
                newFragment = new FragmentJoytvDrama();
                args.putString("baseUrl", fragmentJoytvDrama);
                args.putString("searchUrl", "/bbs/board.php?bo_table=tvdrama&sfl=wr_subject&sop=and&stx=");
                args.putString("type", "%7C1/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LINK_ENTER:
                newFragment = new FragmentJoytvDrama();
                args.putString("baseUrl", fragmentJoytvEnter);
                args.putString("searchUrl", "/bbs/board.php?bo_table=tvyeneng&sfl=wr_subject&sop=and&stx=");
                args.putString("type", "%7C2/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_BAY_MUBI:
                newFragment = new FragmentJoytvDrama();
                args.putString("baseUrl", fragmentJoytvSisa);
                args.putString("searchUrl", "/bbs/board.php?bo_table=tvsisa&sfl=wr_subject&sop=and&stx=");
                args.putString("type", "%7C4/page/");
                newFragment.setArguments(args);
                break;
                /*
            case FRAGMENT_LINK_SISA:
                newFragment = new FragmentLinkMid();
                args.putString("baseUrl", fragmentLinkSisa);
                args.putString("type", "%7C3/page/");
                newFragment.setArguments(args);
                break;

                 */
            case FRAGMENT_LINK_MID:
                newFragment = new FragmentLinkMid();
                args.putString("baseUrl", fragmentLinkMid);
                args.putString("type", "%7C6/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LINK_JOONGD:
                newFragment = new FragmentLinkMid();
                args.putString("baseUrl", fragmentLinkJoongd);
                args.putString("type", "%7C8/page/");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ATKOR_MUBI_HAN_CLASSIC:
                newFragment = new FragmentAtkorMu();
                args.putString("baseUrl", fragmentAtkorMubiHanClassic);
                args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ATKOR_MUBI_OVER_CLASSIC:
                newFragment = new FragmentAtkorMu();
                args.putString("baseUrl", fragmentAtkorMubiOverClassic);
                args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ATKOR_MID:
                newFragment = new FragmentAtkorMid();
                args.putString("baseUrl", fragmentAtkorMid);
                args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ATKOR_JOONGD:
                newFragment = new FragmentAtkorMid();
                args.putString("baseUrl", fragmentAtkorJoongd);
                args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_ATKOR_ILD:
                newFragment = new FragmentAtkorMid();
                args.putString("baseUrl", fragmentAtkorIld);
                args.putString("searchUrl", "&sop=and&sca=&sfl=wr_subject&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_MUBI_HAN:
                newFragment = new FragmentKorMubi();
                args.putString("baseUrl", fragmentKorMubiHan);
                args.putString("searchUrl", "&bo_table=kmovie&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_MUBI_OVER:
                newFragment = new FragmentKorMubi();
                args.putString("baseUrl", fragmentKorMubiOver);
                args.putString("searchUrl", "&bo_table=engmovie&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_MID:
                newFragment = new FragmentKorMid();
                args.putString("baseUrl", fragmentKorMid);
                args.putString("searchUrl", "&bo_table=mid&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_CD:
                newFragment = new FragmentKorMid();
                args.putString("baseUrl", fragmentKorCd);
                args.putString("searchUrl", "&bo_table=cd&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_KOR_JD:
                newFragment = new FragmentKorMid();
                args.putString("baseUrl", fragmentKorJd);
                args.putString("searchUrl", "&bo_table=jd&sca=&sfl=wr_subject&sop=and&stx=");
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LIVE:
                newFragment = new FragmentLive();
                args.putString("baseUrl", fragmentLive);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LIVE_ENTER:
                newFragment = new FragmentLive();
                args.putString("baseUrl", fragmentLiveEnter);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_LIVE_SPORTS:
                newFragment = new FragmentLive();
                args.putString("baseUrl", fragmentLiveSports);
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FAKE_MENU:
                newFragment = new FakeFragment();
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FAKE_ONE:
                newFragment = new FragmentReviewHand();
                args.putString("paramCty", CommonMessage.VAL_KO);   // 한국
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FAKE_TWO:
                newFragment = new FragmentReviewHand();
                args.putString("paramCty", CommonMessage.VAL_CH);   // 중국
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FAKE_THREE:
                newFragment = new FragmentReviewHand();
                args.putString("paramCty", CommonMessage.VAL_TA);   // 대만
                newFragment.setArguments(args);
                break;
            case FRAGMENT_FAKE_FOUR:
                newFragment = new FragmentReviewHand();
                args.putString("paramCty", CommonMessage.VAL_JP);   // 일본
                newFragment.setArguments(args);
                break;
            default:
                Log.d(TAG, "Unhandle case");
                //newFragment = new FragmentMaruSearch2();
                //args.putString("baseUrl", fragmentMaruSearch);
                newFragment = new FragmentBayIld();
                args.putString("baseUrl", fragmentMaruDrama);
                newFragment.setArguments(args);
                break;
        }

        return newFragment;
    }

    public void fragmentReplace(int reqNewFragmentIndex) {
        Fragment newFragment = null;
        Log.d(TAG, "fragmentReplace " + reqNewFragmentIndex);
        newFragment = getFragment(reqNewFragmentIndex);
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction(); // replace fragment
        transaction.replace(R.id.ll_fragment, newFragment);
        transaction.commit();   // Commit the transaction
    }

    public void offColorTv(){
        tv_fragment01.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment02.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment03.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment04.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragment05.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentMaruDrama.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentMaruEnter.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentBayMubi.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentLinkDrama.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentLinkEnter.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentLinkSisa.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentAtkorMubiHanClassic.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentAtkorMubiOverClassic.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentAtkorMid.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentAtkorJoongd.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentAtkorIld.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorMubiHan.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorMubiOver.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorMid.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorCd.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentKorJd.setBackgroundResource(R.drawable.fragment_borther);
    }

    public void offColorTv2(){
        tv_fragmentFakeOne.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentFakeTwo.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentFakeThree.setBackgroundResource(R.drawable.fragment_borther);
        tv_fragmentFakeFour.setBackgroundResource(R.drawable.fragment_borther);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "main activity onResume, appStatus : " + appStatus);

        if(appStatus.equals("1") && d1 != null){
            d2 = new Date();
            long diff = d2.getTime() - d1.getTime();
            long min = diff / 1000 / 60 ;
            //Log.d(TAG, "min : " + min);
            if(min >= 60 && adsFlag.equals("1")){
                d1 = new Date();

                interstitialAd = new InterstitialAd(this, "613cd10065a17f6519f34f21");
                interstitialAd.setAdListener(
                        new com.gomfactory.adpie.sdk.InterstitialAd.InterstitialAdListener() {
                            @Override
                            public void onAdLoaded() {
                                if (interstitialAd.isLoaded()) {
                                    mHandler = new Handler();
                                    mHandler.postDelayed(new Runnable() {
                                        public void run() {
                                            interstitialAd.show();
                                        }
                                    }, 4000); // 0.5초후
                                }
                            }
                            @Override
                            public void onAdFailedToLoad(int errorCode) { }
                            @Override
                            public void onAdShown() { }
                            @Override
                            public void onAdClicked() {  }
                            @Override
                            public void onAdDismissed() {  }
                        }
                );
                interstitialAd.load();

            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Log.d(TAG, "close adsFlag : " + adsFlag);

            interstitialAd = new InterstitialAd(this, "613cd10065a17f6519f34f21");
            interstitialAd.setAdListener(
                    new com.gomfactory.adpie.sdk.InterstitialAd.InterstitialAdListener() {
                        @Override
                        public void onAdLoaded() {
                            if (interstitialAd.isLoaded()) {

                            }
                        }
                        @Override
                        public void onAdFailedToLoad(int errorCode) { }
                        @Override
                        public void onAdShown() { }
                        @Override
                        public void onAdClicked() { finish(); }
                        @Override
                        public void onAdDismissed() { finish(); }
                    }
            );
            interstitialAd.load();

            mHandler = new Handler();
            mHandler.postDelayed(new Runnable() {
                public void run() {
                    showDefaultClosePopup();
                }
            }, 1000); // 0.5초후

            /*
            if (mCloseAd.isModuleLoaded() && adsFlag.equals("1")) {
                Log.d(TAG, "success close ad");

                //mCloseAd.show(this);

                showDefaultClosePopup();
            } else {
                // 광고에 필요한 리소스를 한번만  다운받는데 실패했을 때 앱의 종료팝업 구현
                showDefaultClosePopup();
                Log.d(TAG, "fail close ad");
            }
            */
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showDefaultClosePopup() {
        new AlertDialog.Builder(this).setTitle("").setMessage("종료 하시겠습니까?")
                .setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if(appStatus.equals("1")){
                            if (interstitialAd.isLoaded()) {
                                interstitialAd.show();
                            }
                        } else {
                            finish();
                        }

                    }
                })
                .setNegativeButton("아니요", null)
                .show();
    }

    @Override
    protected void onDestroy() {
        if (adView != null) {
            adView.destroy();
            adView = null;
        }

        super.onDestroy();
    }


}