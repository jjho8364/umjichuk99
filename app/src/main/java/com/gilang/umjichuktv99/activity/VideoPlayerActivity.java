package com.gilang.umjichuktv99.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.gilang.umjichuktv99.R;

public class VideoPlayerActivity extends Activity {
    private String TAG = "VideoPlayerActivity - ";
    private WebView webView;
    private String baseUrl = "";
    private String touch = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED, WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        setContentView(R.layout.activity_video_player);

        baseUrl = getIntent().getStringExtra("baseUrl");
        Log.d(TAG, "Video baseUrl : " + baseUrl);

        webView = (WebView)findViewById(R.id.webView);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportMultipleWindows(true); // prevent popup
        //webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setAppCacheMaxSize( 10 * 1024 * 1024 );
        webView.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath() );
        webView.getSettings().setAllowFileAccess( true );
        webView.getSettings().setCacheMode( WebSettings.LOAD_DEFAULT );
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setSaveFormData(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setBackgroundColor(0x00000000);
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) { return null; }
            /////////////////////////////////////////////////////////////////////////////////////
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "Processing webview url click..." + url);
                webView.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) { }
        });

        if(baseUrl.contains("pandora") || baseUrl.contains("mixdrop")){
            Toast.makeText(this,"pandorad와 mixdrop은 웹브라우저에서만 시청 가능합니다.", Toast.LENGTH_LONG);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(baseUrl));
            startActivity(intent);
            finish();
        } else {
            webView.loadUrl(AddHttps(baseUrl));
        }

    }

    public String AddHttps(String baseUrl){
        String resultUrl = baseUrl;
        if(!baseUrl.contains("http")) resultUrl = "https:" + resultUrl;

        return resultUrl;
    }
}