package com.gilang.umjichuktv99.activity.drama.searchotg;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gilang.umjichuktv99.R;
import com.gilang.umjichuktv99.activity.VideoPlayerActivity;
import com.gilang.umjichuktv99.activity.drama.tvusan.TvusanBtnListActivity;
import com.gilang.umjichuktv99.adapter.DramaBtnAdapter;
import com.gilang.umjichuktv99.item.GridDramaItem;
import com.gilang.umjichuktv99.util.CommonFunction;
import com.gomfactory.adpie.sdk.AdView;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class OtgBtnKistActivity extends Activity {
    private String TAG = "OtgBtnKistActivity - ";
    private ProgressDialog mProgressDialog;
    private GetBtnList getBtnList = null;
    private GetPlayer getPlayer = null;
    private String baseUrl = "";
    private String title = "";
    private String imgUrl = "";
    private TextView tv_title;
    private ImageView img_poster;

    private ArrayList<GridDramaItem> listViewItemArr;
    private ListView btnListView;

    private String nextUrl = "";
    String iframeUrl = "";

    private int adsCnt = 0;
    private String listUrl = "";

    // 중복 클릭 방지 시간 설정
    private static final long MIN_CLICK_INTERVAL=800;
    private long mLastClickTime;

    SharedPreferences pref;

    private String adsFlag = "";

    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_hodu_list);

        adView = (AdView) findViewById(R.id.ad_view);
        adView.setScaleUp(true);
        adView.setAutoBgColor(true);
        adView.load();

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        imgUrl = intent.getStringExtra("imgUrl");
        title = intent.getStringExtra("title");

        Log.d(TAG, "baseUrl : " + baseUrl);

        tv_title = (TextView)findViewById(R.id.tv_title);
        img_poster  = (ImageView)findViewById(R.id.img_poster);

        tv_title.setText(title);

        if (imgUrl != null && !imgUrl.equals("")){
            Picasso.with(this).load(imgUrl).into(img_poster);
        }

        btnListView = (ListView)findViewById(R.id.list_btn_view);

        getBtnList = new GetBtnList();
        getBtnList.execute();

    }

    public class GetBtnList extends AsyncTask<Void, Void, Void> {

        ArrayList<String> btnTextArr = new ArrayList<String>();
        ArrayList<String> btnVideoUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<GridDramaItem>();

            mProgressDialog = new ProgressDialog(OtgBtnKistActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            Document docBtn = null;
            try {
                Log.d(TAG, "OtgBtnKistActivity baseUrl : " + baseUrl);
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                Elements elements = doc.select(".pagination li a");

                for(Element element: elements) {
                    String title = element.text();
                    String listUrl = element.attr("href");

                    btnTextArr.add(title);
                    btnVideoUrlArr.add(listUrl);
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            if(OtgBtnKistActivity.this != null && btnTextArr.size() != 0){

                ///////// set button list ////////////
                if(btnTextArr.size() == 0){
                    Toast.makeText(OtgBtnKistActivity.this, "플레이어 링크가 존재하지 않습니다.", Toast.LENGTH_SHORT).show();
                }
                btnListView.setAdapter(new DramaBtnAdapter(btnTextArr));
                btnListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        long currentClickTime= SystemClock.uptimeMillis();
                        long elapsedTime=currentClickTime-mLastClickTime;
                        mLastClickTime=currentClickTime;

                        // 중복 클릭인 경우
                        if(elapsedTime<=MIN_CLICK_INTERVAL){
                            return;
                        }

                        if(mProgressDialog != null) mProgressDialog.dismiss();

                        /*
                        String playerUrl = btnVideoUrlArr.get(position);

                        Intent intent = new Intent(OtgBtnKistActivity.this, VideoPlayerActivity.class);
                        intent.putExtra("baseUrl", CommonFunction.AddHttps(playerUrl));
                        startActivity(intent);
                         */

                        nextUrl = btnVideoUrlArr.get(position);
                        getPlayer = new GetPlayer();
                        getPlayer.execute();

                    }
                });

            }

            if(mProgressDialog != null) mProgressDialog.dismiss();
        }
    }

    public class GetPlayer extends AsyncTask<Void, Void, Void> {

        String playerUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(mProgressDialog != null) mProgressDialog.dismiss();
            mProgressDialog = new ProgressDialog(OtgBtnKistActivity.this);
            mProgressDialog.setTitle("플레이어를 찾는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;

            try {
                //Log.d(TAG, "nextUrl : " + nextUrl);
                doc = Jsoup.connect(nextUrl).timeout(15000).get();
                playerUrl = doc.select(".player iframe").attr("src");

                if(playerUrl == null || playerUrl.equals("")){
                    playerUrl = doc.select(".div-container .div-center a").attr("href");
                }

                if(playerUrl.contains("media.videomovil")){
                    doc = Jsoup.connect(playerUrl).timeout(15000).get();
                    playerUrl = doc.select("iframe").first().attr("src");
                }

                if(playerUrl == null || playerUrl.equals("")){
                    playerUrl = doc.select(".player a").attr("href");
                }

                if(playerUrl == null || playerUrl.equals("")){
                    playerUrl = doc.select(".player a").attr("href");
                }

                if(playerUrl == null || playerUrl.equals("")){
                    playerUrl = doc.select("iframe").attr("src");
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Intent intent = new Intent(OtgBtnKistActivity.this, VideoPlayerActivity.class);
            intent.putExtra("baseUrl", CommonFunction.AddHttps(playerUrl));
            startActivity(intent);

            if(mProgressDialog != null) mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        if (adView != null) {
            adView.destroy();
            adView = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        destroyAsync();
    }

    public void destroyAsync(){
        if(getBtnList != null){
            getBtnList.cancel(true);
        }
        if(getPlayer != null){
            getPlayer.cancel(true);
        }
    }
}
