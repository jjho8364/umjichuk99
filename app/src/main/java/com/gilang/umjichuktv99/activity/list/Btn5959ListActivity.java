package com.gilang.umjichuktv99.activity.list;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.gilang.umjichuktv99.R;
import com.gilang.umjichuktv99.activity.VideoPlayerActivity;
import com.gilang.umjichuktv99.adapter.DramaBtnAdapter;
import com.gomfactory.adpie.sdk.AdView;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class Btn5959ListActivity extends Activity {
    private String TAG = " Btn5959ListActivity - ";
    private ProgressDialog mProgressDialog;
    private GetBtnListUrl getBtnListUrl = null;
    private GetIframUrl getIframUrl = null;
    private String baseUrl = "";
    private String title = "";
    private String imgUrl = "";
    private TextView tv_title;
    private ImageView img_poster;

    private ListView btnListView;
    private String nextUrl = "";
    private int adsCnt = 0;
    private String listUrl = "";
    private String videoUrl = "";

    //private GetIframeUtl = null;
    private String iframeUrl = "";

    SharedPreferences pref;
    private String adsFlag = "";

    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_btnlist_maru);

        adView = (AdView) findViewById(R.id.ad_view);
        adView.setScaleUp(true);
        adView.setAutoBgColor(true);
        adView.load();

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        title = intent.getStringExtra("title");
        imgUrl = intent.getStringExtra("imgUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        tv_title = (TextView)findViewById(R.id.tv_list_title);
        img_poster  = (ImageView)findViewById(R.id.iv_list_poster);
        //tv_story.setMovementMethod(new ScrollingMovementMethod());
        tv_title.setText(title);

        if (imgUrl != null && !imgUrl.equals("")){
            Picasso.with(this).load(imgUrl).into(img_poster);
        }
        btnListView = (ListView)findViewById(R.id.list_btn_view);

        // show bybit
        pref= getSharedPreferences("pref", MODE_PRIVATE); // 선언

        adsFlag = pref.getString("adsFlag",null);

        getBtnListUrl = new GetBtnListUrl();
        getBtnListUrl.execute();


    }

    public class GetBtnListUrl extends AsyncTask<Void, Void, Void> {

        ArrayList<String> btnTextArr = new ArrayList<String>();
        ArrayList<String> btnVideoUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(Btn5959ListActivity.this);
            mProgressDialog.setTitle("링크를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;

            try {
                doc = Jsoup.connect(baseUrl).timeout(15000).get();
                Elements elements = doc.select(".pagination li a");

                for(Element element: elements) {
                    String title = element.text();
                    String pageUrl = element.attr("href");

                    btnTextArr.add(title);
                    btnVideoUrlArr.add(pageUrl);
                }

            } catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            ///////// set button list ////////////
            if(btnTextArr.size() == 0){
                Toast.makeText(Btn5959ListActivity.this, "연결링크가 없습니다. 잠시 후 다시 이용해 주세요.", Toast.LENGTH_SHORT).show();
            }
            btnListView.setAdapter(new DramaBtnAdapter(btnTextArr));
            btnListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    nextUrl = btnVideoUrlArr.get(position);

                    if(getIframUrl != null){
                        getIframUrl.cancel(true);
                    }

                    getIframUrl = new GetIframUrl();
                    getIframUrl.execute();

                }
            });

            if(mProgressDialog != null) mProgressDialog.dismiss();
        }
    }
    
    
    public class GetIframUrl extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(Btn5959ListActivity.this);
            mProgressDialog.setTitle("비디오 주소를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;
            Document btnDoc = null;
            try {
                doc = Jsoup.connect(nextUrl).timeout(15000).get();

                // button
                videoUrl = doc.select(".player iframe").attr("src");

            } catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(mProgressDialog != null) mProgressDialog.dismiss();

            Intent intent = new Intent(Btn5959ListActivity.this, VideoPlayerActivity.class);
            intent.putExtra("baseUrl", videoUrl);
            startActivity(intent);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getBtnListUrl != null){
            getBtnListUrl.cancel(true);
        }
        if(getIframUrl != null){
            getIframUrl.cancel(true);
        }
    }

    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
            adView = null;
        }
        super.onDestroy();
        if(getBtnListUrl != null){
            getBtnListUrl.cancel(true);
        }
        if(getIframUrl != null){
            getIframUrl.cancel(true);
        }
    }

}
