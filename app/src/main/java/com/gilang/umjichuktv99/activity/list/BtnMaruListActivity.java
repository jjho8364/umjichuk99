package com.gilang.umjichuktv99.activity.list;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gilang.umjichuktv99.R;
import com.gilang.umjichuktv99.activity.VideoPlayerActivity;
import com.gilang.umjichuktv99.adapter.DramaBtnAdapter;
import com.gomfactory.adpie.sdk.AdView;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class BtnMaruListActivity extends Activity {
    private String TAG = " BtnMaruListActivity - ";
    private ProgressDialog mProgressDialog;
    private GetStory getStory = null;
    private String baseUrl = "";
    private String title = "";
    private String imgUrl = "";
    private TextView tv_title;
    private ImageView img_poster;
    private ListView btnListView;
    private String nextUrl = "";
    private int adsCnt = 0;
    private String listUrl = "";

    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_btnlist_maru);

        adView = (AdView) findViewById(R.id.ad_view);
        adView.setScaleUp(true);
        adView.setAutoBgColor(true);
        adView.load();

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        title = intent.getStringExtra("title");
        imgUrl = intent.getStringExtra("imgUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        tv_title = (TextView)findViewById(R.id.tv_list_title);
        img_poster  = (ImageView)findViewById(R.id.iv_list_poster);
        tv_title.setText(title);

        if (imgUrl != null && !imgUrl.equals("")){
            Picasso.with(this).load(imgUrl).into(img_poster);
        }
        btnListView = (ListView)findViewById(R.id.list_btn_view);

        getStory = new GetStory();
        getStory.execute();
    }

    public class GetStory extends AsyncTask<Void, Void, Void> {

        String storyStr = "";
        ArrayList<String> btnTextArr = new ArrayList<String>();
        ArrayList<String> btnVideoUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(BtnMaruListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;
            Document btnDoc = null;
            try {
                doc = Jsoup.connect(baseUrl).get();
                // button
                Elements elements = doc.select(".drama_video_body .anime_muti_link a");

                for(Element element: elements) {
                    String title = element.text();
                    String pageUrl = element.attr("data-video");

                    btnTextArr.add(title);
                    btnVideoUrlArr.add(pageUrl);
                }
            } catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ///////// set button list ////////////
            if(btnTextArr.size() == 0){
                Toast.makeText(BtnMaruListActivity.this, "연결링크가 없습니다. 잠시 후 다시 이용해 주세요.", Toast.LENGTH_SHORT).show();
            }
            btnListView.setAdapter(new DramaBtnAdapter(btnTextArr));
            btnListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    nextUrl = btnVideoUrlArr.get(position);
                    Intent intent = new Intent(BtnMaruListActivity.this, VideoPlayerActivity.class);
                    intent.putExtra("baseUrl", nextUrl);
                    startActivity(intent);
                }
            });
            if(mProgressDialog != null) mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        if (adView != null) {
            adView.destroy();
            adView = null;
        }

        super.onDestroy();
    }
}

