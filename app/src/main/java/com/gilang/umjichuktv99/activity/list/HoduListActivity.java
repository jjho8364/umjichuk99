package com.gilang.umjichuktv99.activity.list;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gomfactory.adpie.sdk.AdView;
import com.squareup.picasso.Picasso;
import com.gilang.umjichuktv99.R;
import com.gilang.umjichuktv99.activity.VideoPlayerActivity;
import com.gilang.umjichuktv99.adapter.DramaBtnAdapter;
import com.gilang.umjichuktv99.item.GridDramaItem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class HoduListActivity extends Activity {
    private String TAG = " HoduListActivity - ";
    private ProgressDialog mProgressDialog;
    private GetGridView getGridView = null;
    private String baseUrl = "";
    private String title = "";
    private String imgUrl = "";
    private TextView tv_title;
    private ImageView img_poster;

    private ArrayList<GridDramaItem> listViewItemArr;
    private ListView btnListView;

    private String nextUrl = "";
    private GetPlayer getPlayer;
    String iframeUrl = "";

    private int adsCnt = 0;
    private String listUrl = "";

    // 중복 클릭 방지 시간 설정
    private static final long MIN_CLICK_INTERVAL=800;
    private long mLastClickTime;

    SharedPreferences pref;

    private String adsFlag = "";

    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_hodu_list);

        adView = (AdView) findViewById(R.id.ad_view);
        adView.setScaleUp(true);
        adView.setAutoBgColor(true);
        adView.load();

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        title = intent.getStringExtra("title");
        imgUrl = intent.getStringExtra("imgUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        tv_title = (TextView)findViewById(R.id.tv_title);
        img_poster  = (ImageView)findViewById(R.id.img_poster);

        tv_title.setText(title);

        if (imgUrl != null && !imgUrl.equals("")){
            Picasso.with(this).load(imgUrl).into(img_poster);
        }

        btnListView = (ListView)findViewById(R.id.list_btn_view);

        // show bybit
        pref= getSharedPreferences("pref", MODE_PRIVATE); // 선언

        adsFlag = pref.getString("adsFlag",null);

        getGridView = new GetGridView();
        getGridView.execute();

    }

    public class GetGridView extends AsyncTask<Void, Void, Void> {

        ArrayList<String> btnTextArr = new ArrayList<String>();
        ArrayList<String> btnVideoUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listViewItemArr = new ArrayList<GridDramaItem>();

            mProgressDialog = new ProgressDialog(HoduListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document doc = null;
            Document docBtn = null;
            try {
                //Log.d(TAG, "baseUrl : " + baseUrl);
                doc = Jsoup.connect(baseUrl).timeout(15000).get();

                iframeUrl = doc.select("#player iframe").attr("src");
                //Log.d(TAG, "iframeUrl : " + iframeUrl);
                docBtn = Jsoup.connect(iframeUrl).timeout(15000).get();

                Elements elements = docBtn.select("input");

                for(Element element : elements) {
                    String title = element.attr("id");
                    String listUrl = element.attr("value");

                    btnTextArr.add(title);
                    btnVideoUrlArr.add(listUrl);

                    //Log.d(TAG, "title : " + title);
                }


            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            if(HoduListActivity.this != null && btnTextArr.size() != 0){

                ///////// set button list ////////////
                if(btnTextArr.size() == 0){
                    Toast.makeText(HoduListActivity.this, "방송 준비 중입니다. 1시간 내에 완료됩니다.", Toast.LENGTH_SHORT).show();
                }
                btnListView.setAdapter(new DramaBtnAdapter(btnTextArr));
                btnListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        long currentClickTime= SystemClock.uptimeMillis();
                        long elapsedTime=currentClickTime-mLastClickTime;
                        mLastClickTime=currentClickTime;

                        // 중복 클릭인 경우
                        if(elapsedTime<=MIN_CLICK_INTERVAL){
                            return;
                        }

                        if(mProgressDialog != null) mProgressDialog.dismiss();

                        Intent intent = new Intent(HoduListActivity.this, VideoPlayerActivity.class);
                        intent.putExtra("baseUrl", btnVideoUrlArr.get(position));
                        startActivity(intent);

                        //nextUrl = btnVideoUrlArr.get(position);
                        //getPlayer = new GetPlayer();
                        //getPlayer.execute();
                    }
                });

            }

            if(mProgressDialog != null) mProgressDialog.dismiss();
        }
    }

    public class GetPlayer extends AsyncTask<Void, Void, Void> {

        String playerUrl = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(HoduListActivity.this);
            mProgressDialog.setTitle("플레이어를 찾는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;

            try {
                //Log.d(TAG, "nextUrl : " + nextUrl);
                doc = Jsoup.connect(nextUrl).timeout(15000).get();
                playerUrl = doc.select(".player iframe").attr("src");

                if(playerUrl == null || playerUrl.equals("")){
                    playerUrl = doc.select(".div-container .div-center a").attr("href");
                }

                if(playerUrl.contains("media.videomovil")){
                    doc = Jsoup.connect(playerUrl).timeout(15000).get();
                    playerUrl = doc.select("iframe").first().attr("src");
                }

                if(playerUrl == null || playerUrl.equals("")){
                    playerUrl = doc.select(".player a").attr("href");
                }

                if(playerUrl == null || playerUrl.equals("")){
                    playerUrl = doc.select(".player a").attr("href");
                }

                if(playerUrl == null || playerUrl.equals("")){
                    playerUrl = doc.select("iframe").attr("src");
                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Intent intent = new Intent(HoduListActivity.this, VideoPlayerActivity.class);
            intent.putExtra("baseUrl", playerUrl);
            startActivity(intent);

            if(mProgressDialog != null) mProgressDialog.dismiss();
        }
    }

    public void refresh(String listUrl){
        Intent intent = new Intent(HoduListActivity.this, HoduListActivity.class);
        intent.putExtra("listUrl", listUrl);
        intent.putExtra("title", title);
        intent.putExtra("imgUrl", imgUrl);
        intent.putExtra("adsCnt", ""+adsCnt);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        if (adView != null) {
            adView.destroy();
            adView = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        destroyAsync();
    }

    public void destroyAsync(){
        if(getGridView != null){
            getGridView.cancel(true);
        }
        if(getPlayer != null){
            getPlayer.cancel(true);
        }
    }



}
