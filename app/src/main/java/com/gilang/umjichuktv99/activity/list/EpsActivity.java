package com.gilang.umjichuktv99.activity.list;

import static com.gilang.umjichuktv99.common.CommonMessage.MIN_CLICK_INTERVAL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gomfactory.adpie.sdk.AdView;
import com.squareup.picasso.Picasso;
import com.gilang.umjichuktv99.R;
import com.gilang.umjichuktv99.activity.VideoPlayerActivity;
import com.gilang.umjichuktv99.adapter.ListViewEpsAdapter;
import com.gilang.umjichuktv99.common.CommonMessage;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class EpsActivity extends Activity implements View.OnClickListener {
    private String TAG = " EpsActivity - ";
    private ProgressDialog mProgressDialog;

    // 중복 클릭 방지 시간 설정
    private long mLastClickTime;

    private String baseUrl = "";
    private String title = "";
    private String imgUrl = "";
    private String idNo = "";
    private String story = "";

    private TextView tv_eps_title;
    private TextView tv_eps_sec_title;
    private TextView tv_eps_date;
    private TextView tv_eps_gen;
    private TextView tv_eps_starring;
    private TextView tv_eps_story;
    private ImageView img_eps_poster;

    private ListView listView;

    private int adsCnt = 0;
    private int adn = 0;
    private String epsUrl = "";

    private String startEps = "1";
    private String endEps = "20";

    GetEpsInfo getEpsInfo;
    int viewCnt = 0;
    SharedPreferences pref;

    String selectedVideoUrl = "";

    private String strElements = "";
    private String strDate = "";
    private String strTitle = "";

    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_eps);

        adView = (AdView) findViewById(R.id.ad_view);
        adView.setScaleUp(true);
        adView.setAutoBgColor(true);
        adView.load();

        pref= getSharedPreferences("pref", MODE_PRIVATE);

        strElements = "bXTlJJ";//pref.getString("strElements","bXTlJJ");
        strDate = "ewOJjk";//pref.getString("strDate","ewOJjk");
        strTitle = "jlgipK";//pref.getString("strTitle","jlgipK");

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("epsUrl");
        imgUrl = intent.getStringExtra("imgUrl");
        title = intent.getStringExtra("title");
        idNo = intent.getStringExtra("idNo");

        tv_eps_title = (TextView)findViewById(R.id.tv_eps_title);
        tv_eps_sec_title = (TextView)findViewById(R.id.tv_eps_sec_title);
        tv_eps_date = (TextView)findViewById(R.id.tv_eps_date);
        tv_eps_gen = (TextView)findViewById(R.id.tv_eps_gen);
        tv_eps_starring = (TextView)findViewById(R.id.tv_eps_starring);
        tv_eps_story =  (TextView)findViewById(R.id.tv_eps_story);
        img_eps_poster = (ImageView)findViewById(R.id.iv_eps_poster);

        listView = (ListView)findViewById(R.id.list_eps_view);

        if(imgUrl==null || imgUrl.equals("") || !imgUrl.contains("https:")){
            img_eps_poster.setImageResource(R.drawable.noimage);
        } else {
            Picasso.with(this).load(imgUrl).into(img_eps_poster);
        }
        tv_eps_title.setText(title);

        getEpsInfo = new GetEpsInfo();
        getEpsInfo.execute();

    }

    public class GetEpsInfo extends AsyncTask<Void, Void, Void> {

        String secTitle = "";
        String date = "";
        String gen = "";
        String starring = "";

        ArrayList<String> listTitleArr = new ArrayList<String>();
        ArrayList<String> listimgArr = new ArrayList<String>();
        ArrayList<String> listvideoUrlArr = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            mProgressDialog = new ProgressDialog(EpsActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            Document doc = null;
            Document docStory = null;
            Document docTeaserList = null;

            try {
                //Log.d(TAG, "baseUrl : " + baseUrl);
                doc = Jsoup.connect(baseUrl)
                        .header(CommonMessage.headerAccLang, CommonMessage.headerLang)
                        .get();

                Elements elements = doc.select("main ." + strElements);

                secTitle = doc.select("main ." + strTitle).text();
                date = "출시 : " + doc.select("main ." + strDate).text() + " " + doc.select("main .t7lodv-1.eXnMcX").text();
                gen = "장르 : " + elements.get(0).select("a").text();
                starring = "출연 : " + elements.get(1).select("a").text();

                //Log.d(TAG, "secTitle : " + secTitle);
                //Log.d(TAG, "date : " + date);
                //Log.d(TAG, "gen : " + gen);
                //Log.d(TAG, "starring : " + starring);

                // 줄거리 가져오기
                //Log.d(TAG, "story url : " + CommonMessage.getStroyUrl1 + idNo + CommonMessage.getStroyUrl2);
                docStory = Jsoup.connect(CommonMessage.getStroyUrl1 + idNo + CommonMessage.getStroyUrl2)
                        .referrer(baseUrl)
                        .header(CommonMessage.headerAccLang, CommonMessage.headerLang)
                        .ignoreContentType(true)
                        .get();

                String getFullJson = docStory.select("body").text();
                JSONObject fullJson = new JSONObject(getFullJson);

                JSONObject storyJson = new JSONObject(fullJson.get("descriptions")+"");

                if(storyJson.has("ko")) {
                    story = "줄거리 : " + storyJson.get("ko") + "";
                } else  if(storyJson.has("en")) {
                    story = "story : " + storyJson.get("en") + "";
                } else {
                    story = "한글 및 영어 스토리가 없습니다.";
                }

                // 티저 가져오기
                //Log.d(TAG,"epsUrl : " + CommonMessage.getTeaserUrl1 + idNo + CommonMessage.getTeaserUrl2);
                docTeaserList = Jsoup.connect(CommonMessage.getTeaserUrl1 + idNo + CommonMessage.getTeaserUrl2)
                        .referrer(baseUrl)
                        .header(CommonMessage.headerAccLang, CommonMessage.headerLang)
                        .ignoreContentType(true)
                        .get();

                JSONObject fullTeaserJson = new JSONObject(docTeaserList.select("body").text());
                JSONArray jsonTeaserArr = new JSONArray(fullTeaserJson.get("response").toString());

                String TeaserVideoUrl = "";
                String TeaserImgUrl = "";

                for(int i=0 ; i<jsonTeaserArr.length() ; i++) {
                    JSONObject tempJson = new JSONObject(jsonTeaserArr.get(i).toString());

                    JSONObject urlJson = new JSONObject(tempJson.get("url").toString());
                    TeaserVideoUrl = urlJson.get("web").toString();
                    //Log.d(TAG, "TeaserVideoUrl : " + TeaserVideoUrl);

                    listTitleArr.add("Teaser " + i);
                    listvideoUrlArr.add(TeaserVideoUrl);

                    // get image
                    JSONObject imageJson = new JSONObject(tempJson.get("images").toString());
                    JSONObject posterJson = new JSONObject(imageJson.get("poster").toString());
                    TeaserImgUrl = posterJson.get("url").toString();
                    listimgArr.add(TeaserImgUrl);
                    //Log.d(TAG, "imgUrl : " + imgUrl);

                }

            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(this != null){

                tv_eps_sec_title.setText(secTitle);
                tv_eps_date.setText(date);
                tv_eps_gen.setText(gen);
                tv_eps_starring.setText(starring);
                tv_eps_story.setText(story);
                tv_eps_story.setMovementMethod(new ScrollingMovementMethod());

                listView.setAdapter(new ListViewEpsAdapter(EpsActivity.this, 1, listimgArr, listvideoUrlArr, R.layout.item_eps_listview));
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        long currentClickTime = SystemClock.uptimeMillis();
                        long elapsedTime = currentClickTime - mLastClickTime;
                        mLastClickTime = currentClickTime;

                        // 중복 클릭인 경우
                        if (elapsedTime <= MIN_CLICK_INTERVAL) {
                            return;
                        }

                        //pref= getSharedPreferences("pref", MODE_PRIVATE);
                        viewCnt = pref.getInt("viewCnt",0);

                        selectedVideoUrl = listvideoUrlArr.get(position);
                        //Log.d(TAG, "selectedVideoUrl : " + selectedVideoUrl);

                        if(viewCnt != 0 && viewCnt%2 == 0) {

                            SharedPreferences.Editor editor = pref.edit();
                            editor.putInt("viewCnt", 0); // init
                            editor.commit();
                        } else {
                            Intent intent = new Intent(EpsActivity.this, VideoPlayerActivity.class);
                            intent.putExtra("baseUrl", selectedVideoUrl);
                            intent.putExtra("touch", "1");
                            startActivity(intent);
                        }
                    }
                });

            }

            if(mProgressDialog != null) mProgressDialog.dismiss();

        }
    }

    @Override
    public void onClick(View v) {
        long currentClickTime= SystemClock.uptimeMillis();
        long elapsedTime=currentClickTime-mLastClickTime;
        mLastClickTime=currentClickTime;

        // 중복 클릭인 경우
        if(elapsedTime<=MIN_CLICK_INTERVAL){
            return;
        }

        switch (v.getId()){

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(getEpsInfo != null){
            getEpsInfo.cancel(true);
        }
    }

    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
            adView = null;
        }
        super.onDestroy();
        if(getEpsInfo != null){
            getEpsInfo.cancel(true);
        }
    }

    public void adFull(){

    }
}