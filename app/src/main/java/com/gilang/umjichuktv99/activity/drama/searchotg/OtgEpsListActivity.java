package com.gilang.umjichuktv99.activity.drama.searchotg;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gilang.umjichuktv99.R;
import com.gilang.umjichuktv99.activity.list.MaruEpsListActivity2;
import com.gomfactory.adpie.sdk.AdView;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class OtgEpsListActivity extends Activity implements View.OnClickListener {
    private String TAG = "OtgEpsListActivity - ";
    private ProgressDialog mProgressDialog;
    private GetListView getListView = null;
    private String baseUrl = "";
    private String title = "";
    private String imgUrl = "";
    private TextView tv_title;
    private ImageView img_poster;
    private ListView listView;
    private LinearLayout youtubeAd;

    private int adsCnt = 0;
    private String listUrl = "";

    private String nextUrl = "";
    private String nextTitle = "";
    private String nextImgUrl = "";
    private GetIframe getIframe;

    //// paging ////
    private int pageNum = 1;
    private TextView tv_currentPage;
    private TextView tv_lastPage;
    private Button nextBtn;
    private Button preBtn;
    private String lastPage = "1";
    private boolean getLastFlag = true;

    List<String> listTitleArr;
    List<String> listImgUrlArr;
    List<String> listPageUrlArr;
    ArrayAdapter adapter;

    // 중복 클릭 방지 시간 설정
    private static final long MIN_CLICK_INTERVAL=800;
    private long mLastClickTime;

    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_maru_eps_list);

        adView = (AdView) findViewById(R.id.ad_view);
        adView.setScaleUp(true);
        adView.setAutoBgColor(true);
        adView.load();

        Intent intent = getIntent();
        baseUrl = intent.getStringExtra("listUrl");
        title = intent.getStringExtra("title");
        imgUrl = intent.getStringExtra("imgUrl");
        adsCnt = Integer.parseInt(intent.getStringExtra("adsCnt"));

        tv_title = (TextView)findViewById(R.id.tv_title);
        img_poster  = (ImageView)findViewById(R.id.img_poster);
        tv_title.setText(title);

        if (imgUrl != null && !imgUrl.equals("")){
            Picasso.with(this).load(imgUrl).into(img_poster);
        }

        listView = (ListView)findViewById(R.id.list_listview);

        //// paging ////
        tv_currentPage = (TextView)findViewById(R.id.fr01_currentpage);
        tv_lastPage = (TextView)findViewById(R.id.fr01_lastpage);
        tv_currentPage.setText("1");
        tv_lastPage.setText("1");
        preBtn = (Button)findViewById(R.id.fr01_prebtn);
        nextBtn = (Button)findViewById(R.id.fr01_nextbtn);
        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);

        getListView = new GetListView();
        getListView.execute();

        }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            listTitleArr = new ArrayList<String>();
            listImgUrlArr = new ArrayList<String>();
            listPageUrlArr = new ArrayList<String>();

            mProgressDialog = new ProgressDialog(OtgEpsListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {

        Document doc = null;


        try {
            Log.d(TAG, "baseUrl : " + baseUrl + "page/" + pageNum + "/");
            doc = Jsoup.connect(baseUrl + "page/"  + pageNum).timeout(15000).get();

            Elements elements = doc.select(".video-section .item");

            for(Element element: elements) {

                String title = element.select("h3").text();
                String listUrl = element.select("h3 a").attr("href");
                String imgUrl = element.select(".item-img img").attr("src");

                Log.d(TAG, "title : " + title);

                listTitleArr.add(title);
                listImgUrlArr.add(imgUrl);
                listPageUrlArr.add(listUrl);
            }

            ////////////// get lat page /////////////
            if(lastPage.equals("1") && getLastFlag){
                if(lastPage.equals("1")){
                    Elements elements2 = doc.select(".pagination li");
                    //Log.d(TAG, "elements2 size : " + elements2.size());
                    if(elements2.size() < 3) {
                        lastPage = "1";
                    } else {
                        lastPage = elements2.get(elements2.size()-2).text();
                    }
                }
            }
        } catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        //// paging ////
        tv_currentPage.setText(pageNum+"");
        tv_lastPage.setText(lastPage);

        if(OtgEpsListActivity.this != null && listTitleArr.size() != 0){
            adapter = new ArrayAdapter<String>(OtgEpsListActivity.this, android.R.layout.simple_list_item_1, listTitleArr);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    long currentClickTime= SystemClock.uptimeMillis();
                    long elapsedTime=currentClickTime-mLastClickTime;
                    mLastClickTime=currentClickTime;

                    // 중복 클릭인 경우
                    if(elapsedTime<=MIN_CLICK_INTERVAL){
                        return;
                    }

                    adsCnt++;

                    nextUrl = listPageUrlArr.get(position);
                    nextTitle = listTitleArr.get(position);
                    nextImgUrl = listImgUrlArr.get(position);

                    getIframe = new OtgEpsListActivity.GetIframe();
                    getIframe.execute();
                }
            });
        }

        if(mProgressDialog != null) mProgressDialog.dismiss();
    }
}

public class GetIframe extends AsyncTask<Void, Void, Void> {

    String iframeUrl = "";

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if(mProgressDialog != null) mProgressDialog.dismiss();
        mProgressDialog = new ProgressDialog(OtgEpsListActivity.this);
        mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {

        Document doc = null;
        try {
            Log.d(TAG, "nextUrl : " + nextUrl);
            doc = Jsoup.connect(nextUrl).timeout(20000).get();

            iframeUrl = doc.select(".MVepisodebt").attr("href");

        } catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if(this != null){
            Intent intent = new Intent(OtgEpsListActivity.this, OtgBtnKistActivity.class);
            intent.putExtra("listUrl", iframeUrl);
            intent.putExtra("title", nextTitle);
            intent.putExtra("imgUrl", nextImgUrl);
            intent.putExtra("adsCnt", "0");
            startActivity(intent);
        }

        if(mProgressDialog != null) mProgressDialog.dismiss();
    }
}

    @Override
    protected void onPause() {
        super.onPause();
        destroyAsync();
    }

    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
            adView = null;
        }
        super.onDestroy();

        destroyAsync();
    }

    public void destroyAsync(){
        if(getListView != null){
            getListView.cancel(true);
        }
        if(getIframe != null){
            getIframe.cancel(true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr01_prebtn :
                if(pageNum != 1){
                    pageNum--;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    if(getIframe != null){
                        getIframe.cancel(true);
                    }
                    getListView = new OtgEpsListActivity.GetListView();
                    getListView.execute();
                }
                break;

            case R.id.fr01_nextbtn :
                if(tv_lastPage.getText() != null && !(tv_lastPage.getText().toString().equals("")) && pageNum != Integer.parseInt(tv_lastPage.getText().toString())){
                    pageNum++;
                    if(getListView != null){
                        getListView.cancel(true);
                    }
                    if(getIframe != null){
                        getIframe.cancel(true);
                    }
                    getListView = new OtgEpsListActivity.GetListView();
                    getListView.execute();
                }
                break;
        }
    }

}