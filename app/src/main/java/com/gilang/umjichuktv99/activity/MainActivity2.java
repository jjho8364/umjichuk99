package com.gilang.umjichuktv99.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.gilang.umjichuktv99.R;
import com.gomfactory.adpie.sdk.AdPieError;
import com.gomfactory.adpie.sdk.AdPieSDK;
import com.gomfactory.adpie.sdk.AdView;
import com.gomfactory.adpie.sdk.InterstitialAd;

public class MainActivity2 extends Activity  {

    // adpie
    private AdView adView;

    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        AdPieSDK.getInstance().initialize(getApplicationContext(), "612ee73365a17f6467d5dcb2");

        adView = (AdView) findViewById(R.id.ad_view);
        adView.setSlotId("613cdfe765a17f6519f34f24");
        adView.setScaleUp(true);
        adView.setAutoBgColor(true);
        adView.setAdListener(new com.gomfactory.adpie.sdk.AdView.AdListener() {

             @Override
             public void onAdLoaded() {
                 // 광고 표출 성공 후 이벤트 발생
                 Log.d("aaaa : ", "success adpie baaner");
             }

             @Override
             public void onAdFailedToLoad(int errorCode) {
                 // 광고 요청 또는 표출 실패 후 이벤트 발생
                 // error message -> AdPieError.getMessage(errorCode)
                 Log.d("aaaa : ", "failed adpie baaner");
                 Log.d("error message : ", AdPieError.getMessage(errorCode));
             }

             @Override
             public void onAdClicked() {
                 // 광고 클릭 후 이벤트 발생
                 Log.d("aaaa : ", "clicked adpie baaner");
             }
         });
        adView.load();

        interstitialAd = new InterstitialAd(this, "612ee90b65a17f6467d5dcb6");
        interstitialAd.setAdListener(
                new com.gomfactory.adpie.sdk.InterstitialAd.InterstitialAdListener() {
                    @Override
                    public void onAdLoaded() {
                        // 광고 로딩 완료 후 이벤트 발생

                        // 광고 요청 후 즉시 노출하고자 할 경우 아래의 코드를 추가한다.
                        if (interstitialAd.isLoaded()) {
                            // 광고 표출
                            interstitialAd.show();
                        }
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        // 광고 요청 실패 후 이벤트 발생
                        // error message -> AdPieError.getMessage(errorCode)
                        Log.d("bbbb : ", "failed adpie baaner");
                        Log.d("error message : ", AdPieError.getMessage(errorCode));
                    }

                    @Override
                    public void onAdShown() {
                        // 광고 표출 후 이벤트 발생
                    }

                    @Override
                    public void onAdClicked() {
                        // 광고 클릭 후 이벤트 발생
                    }

                    @Override
                    public void onAdDismissed() {
                        // 광고가 표출한 뒤 사라질 때 이벤트 발생
                    }
                }
        );
        interstitialAd.load();

    }


}