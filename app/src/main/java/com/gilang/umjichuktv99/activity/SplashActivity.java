package com.gilang.umjichuktv99.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.gilang.umjichuktv99.R;
import com.gilang.umjichuktv99.util.HttpUtil;
import com.gomfactory.adpie.sdk.AdPieSDK;
import com.google.gson.JsonObject;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class SplashActivity extends Activity {
    private String TAG = "SplashActivity - ";
    private GetStatus getStatus = null;
    private String splashInfo3 = "";
    private String tistoryUrl = "";
    private int tistoryLastPage = 0;
    private int tistoryRndPage = 0;
    private TextView tvSplashInfo;

    SharedPreferences pref;

    private String baseUrl = "";
    private String[] baseUrlArr = new String[]{
            //"https://teddyzaffran.tistory.com/2"
            "https://teddyzaffran.tistory.com/1",
            "https://findfoodbaru.tistory.com/1",
            "https://sherlong.tistory.com/1",
            "https://koreaskincare.tistory.com/1"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        // init
        //AdPieSDK.getInstance().initialize(getApplicationContext(), "612ee73365a17f6467d5dcb2");

        tvSplashInfo = (TextView)findViewById(R.id.splash_info3);

        baseUrl = baseUrlArr[(int)(Math.random() * baseUrlArr.length)];

        //Log.d(TAG, "selected baseUrl : " + baseUrl);

        pref= getSharedPreferences("pref", MODE_PRIVATE); // 선언


        JsonObject params = new JsonObject();
        params.addProperty("appNm", "umjichuktv");
        params.addProperty("appVs", "3");
        HttpUtil.callApi(params,"GET");



        //getStatus = new GetStatus();
        //getStatus.execute();
    }

    public class GetStatus extends AsyncTask<Void, Void, Void> {

        String appStatus = "";
        String stageNum = "";
        String fragment01Url = "";
        String fragment02Url = "";
        String fragment03Url = "";
        String fragment04Url = "";
        String fragment05Url = "";
        String fragMaruDrama2 = "";
        String fragMaruEnter2 = "";
        String fragMaruSearch = "";
        String fragBayDrama = "";
        String fragBayEnter = "";
        String fragBayMubi = "";
        String fragBayIld = "";
        String fragLinkDrama = "";
        String fragLinkSisa = "";
        String fragLinkEnter = "";
        String fragLinkMid = "";
        String fragLinkJoongd = "";
        String fragAtkorMubi = "";
        String fragAtkorMid = "";
        String fragAtkorJoongd = "";
        String fragAtkorIld = "";
        String fragKorMubiHan = "";
        String fragKorMubiOver = "";
        String fragKorMid = "";
        String fragKorCd = "";
        String fragKorJd = "";
        String fragAtkorMubiHanClassic = "";
        String fragAtkorMubiOverClassic = "";
        String fragLiveTv = "";
        String fragLiveTvEnter = "";
        String fragLiveTvSports = "";

        String maintenanceImgUrl = "";
        String closedImgUrl = "";
        String nextAppUrl = "";

        private String adsFlag = "";
        boolean isConn = true;

        private String fragmentDramaJson;
        private String fragmentEnterJson;
        private String fragmentSisaJson;
        private String fragmentJoytvDrama;
        private String fragmentJoytvEnter;
        private String fragmentJoytvSisa;

        private String fragmentEndContentsOtg;
        private String fragmentEndContentsTvnara;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            Document tistoryDoc = null;
            Document doc = null;

            try {




                tistoryDoc = Jsoup.connect(baseUrl).timeout(15000).get();

                //Log.d(TAG, "1111 : " + tistoryDoc);

                String strHtml = tistoryDoc.select("b").text();

                //Log.d(TAG, "1111 : " + strHtml);

                String reStrHtml = strHtml.replace("<^","<");

                //Log.d(TAG, "2222 : " + reStrHtml);

                //doc = Jsoup.parse(strHtml);
                doc = Jsoup.parse(reStrHtml);

                //Log.d(TAG, "3333 : " + doc);

                appStatus = doc.select(".status.umjichucktv99").text();
                //appStatus = doc.select(".test").text();
                Log.d(TAG, "splash appStatus : " + appStatus);

                splashInfo3 = doc.select(".splash_info_umjichuktv99").text();

                String[] tempStrCoder = doc.select(".coderstorage").text().split(",");
                tistoryUrl = tempStrCoder[0];
                tistoryLastPage = Integer.parseInt(tempStrCoder[1].trim());
                //Log.d(TAG, "tistoryUrl : " + tistoryUrl);
                //Log.d(TAG, "tistoryLastPage : " + tistoryLastPage);

                // 랜덤 페이지 가져오기
                tistoryRndPage = ((int)(Math.random() * (tistoryLastPage-1))) + 2;

                adsFlag = doc.select(".umjichucktv99Ads").text();

                SharedPreferences.Editor editor = pref.edit();
                editor.putString("adsFlag", adsFlag);
                editor.putString("adsCnt", "1");             // init

                // fixedAppStatus를 가져온다.
                String fixedAppStatus = pref.getString("fixedAppStatus",null);

                //Log.d(TAG, "spalsh fixedAppStatus : " + fixedAppStatus);

                if(fixedAppStatus == null || fixedAppStatus.equals("")) {
                    editor.putString("fixedAppStatus", appStatus);
                } else if(appStatus.equals("3")) {
                    appStatus = "3";
                } else if(fixedAppStatus.equals("1")) {
                    appStatus = "1";
                } else {
                    if(fixedAppStatus.equals("99")) appStatus = "99";
                }

                /*
                if(!appStatus.equals("3")) {
                    appStatus = "1";
                }
                */

                String bitcoinimg  = doc.select(".bitcoinimg").text();
                String bitcoinRefLink = doc.select(".bitcoinRefLink").text();

                editor.putString("bitcoinimg", bitcoinimg);
                editor.putString("bitcoinRefLink", bitcoinRefLink);

                editor.commit();

                //Log.d(TAG, "splash final appStatus1 : " + appStatus);

                if(appStatus.equals("1") || appStatus.equals("2") ){
                    //fragment01Url = doc.select(".fragment01url").text();
                    //fragment02Url = doc.select(".fragment02url").text();
                    //fragment03Url = doc.select(".fragment03url").text();
                    //fragment04Url = doc.select(".fragment04url").text();
                    fragment01Url = doc.select(".fragmentHoduDrama").text();
                    fragment02Url = doc.select(".fragmentHoduEndter").text();
                    fragment03Url = doc.select(".fragmentHoduSisa").text();
                    fragment04Url = doc.select(".fragmentHoduSports").text();
                    fragment05Url = doc.select(".fragmentHoduMovie").text();
                    //fragMaruDrama2 = doc.select(".fragMaruDrama2").text();
                    //fragMaruEnter2 = doc.select(".fragMaruEnter2").text();
                    //fragMaruSearch = doc.select(".fragMaruSearch").text();

                    stageNum = doc.select(".stage").text();

                    if(stageNum.equals("2")){
                        //String mainRnum = doc.select(".fragmentMainRnum2").text();
                        //int rnum = (int)((Math.random()*10000)%(Integer.parseInt(mainRnum)));
                        fragMaruDrama2 = doc.select(".fragmentBayDrama").text();
                        fragMaruEnter2 = doc.select(".fragmentBayEnter").text();
                        fragMaruSearch = doc.select(".fragmentBayDrama").text();
                    } else {
                        String mainRnum = doc.select(".fragmentMainRnum").text();
                        int rnum = (int)((Math.random()*10000)%(Integer.parseInt(mainRnum)));
                        fragMaruDrama2 = doc.select(".fragmentMainDrama" + rnum).text();
                        fragMaruEnter2 = doc.select(".fragmentMainEnter" + rnum).text();
                        fragMaruSearch = doc.select(".fragmentMainDramaS" + rnum).text();
                    }

                    fragBayDrama = doc.select(".fragmentBayDrama").text();
                    fragBayEnter = doc.select(".fragmentBayEnter").text();
                    fragBayMubi = doc.select(".fragmentMubiBay").text();
                    fragBayIld = doc.select(".fragmentMubiDongyoungsang").text();
                    fragLinkDrama = doc.select(".fragLinkDrama").text();
                    fragLinkEnter = doc.select(".fragLinkEnter").text();
                    fragLinkSisa = doc.select(".fragLinkSisa").text();
                    fragLinkMid = doc.select(".fragLinkMid").text();
                    fragLinkJoongd = doc.select(".fragLinkJoongd").text();
                    fragAtkorMubi = doc.select(".fragAtkorMubi").text();
                    fragAtkorMid = doc.select(".fragAtkorMid").text();
                    fragAtkorJoongd = doc.select(".fragAtkorJongd").text();
                    fragAtkorIld = doc.select(".fragAtkorIld").text();
                    fragKorMubiHan = doc.select(".fragKorMubiHan").text();
                    fragKorMubiOver = doc.select(".fragKorMubiOver").text();
                    fragKorMid = doc.select(".fragKorMid").text();
                    fragKorCd = doc.select(".fragKorCd").text();
                    fragKorJd = doc.select(".fragKorJd").text();
                    fragAtkorMubiHanClassic = doc.select(".fragAtkorMubiHanClassic").text();
                    fragAtkorMubiOverClassic = doc.select(".fragAtkorMubiOverClassic").text();
                    fragLiveTv = doc.select(".livetv").text();
                    fragLiveTvEnter = doc.select(".livetventer").text();
                    fragLiveTvSports = doc.select(".livetvsports").text();
                    fragmentDramaJson = doc.select(".fragmentDramaJson3").text();
                    fragmentEnterJson = doc.select(".fragmentEnterJson3").text();
                    fragmentSisaJson = doc.select(".fragmentSisaJson3").text();
                    fragmentJoytvDrama = doc.select(".fragmentJoytvDrama").text();
                    fragmentJoytvEnter = doc.select(".fragmentJoytvEnter").text();
                    fragmentJoytvSisa = doc.select(".fragmentJoytvSisa").text();
                    fragmentEndContentsOtg = doc.select(".fragmentEndContentsOtg").text();
                    fragmentEndContentsTvnara = doc.select(".fragmentEndContentsTvnara").text();
                } else if(appStatus.equals("3")){
                    closedImgUrl = doc.select(".closed").text();
                    nextAppUrl = doc.select(".newappurl").text();
                } else if(appStatus.equals("9")){
                    closedImgUrl = doc.select(".closed").text();
                    nextAppUrl = doc.select(".mid.site").text();
                } else {
                    maintenanceImgUrl = doc.select(".maintenance").text();
                }
            } catch(Exception e){
                appStatus = "99";
                isConn = false;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(!isConn) {
                Toast toast = Toast.makeText(SplashActivity.this,"인터넷 연결 상태를 확인하세요.", Toast.LENGTH_LONG);
                toast.show();
            }

            Log.d(TAG, "splashInfo3 : " + splashInfo3);
            //tvSplashInfo.setText(splashInfo3);

            if(appStatus.equals("1")){
                tvSplashInfo.setText(splashInfo3);
            } else {
                tvSplashInfo.setText("드라마 리뷰 및 트레일러(예정) 영상 모음");
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(getApplication(), MainActivity.class);
                    //Intent intent = new Intent(getApplication(), TestActivity.class);

                    intent.putExtra("appStatus", appStatus);

                    if(appStatus.equals("1") || appStatus.equals("2")){
                        intent.putExtra("fragment01Url", fragment01Url);
                        intent.putExtra("fragment02Url", fragment02Url);
                        intent.putExtra("fragment03Url", fragment03Url);
                        intent.putExtra("fragment04Url", fragment04Url);
                        intent.putExtra("fragment05Url", fragment05Url);
                        intent.putExtra("fragMaruDrama2", fragMaruDrama2);
                        intent.putExtra("fragMaruEnter2", fragMaruEnter2);
                        intent.putExtra("fragMaruSearch", fragMaruSearch);
                        intent.putExtra("fragBayDrama", fragBayDrama);
                        intent.putExtra("fragBayEnter", fragBayEnter);
                        intent.putExtra("fragBayMubi", fragBayMubi);
                        intent.putExtra("fragBayIld", fragBayIld);
                        intent.putExtra("fragLinkDrama", fragLinkDrama);
                        intent.putExtra("fragLinkEnter", fragLinkEnter);
                        intent.putExtra("fragLinkSisa", fragLinkSisa);
                        intent.putExtra("fragLinkMid", fragLinkMid);
                        intent.putExtra("fragLinkJoongd", fragLinkJoongd);
                        intent.putExtra("fragAtkorMubi", fragAtkorMubi);
                        intent.putExtra("fragAtkorMid", fragAtkorMid);
                        intent.putExtra("fragAtkorJoongd", fragAtkorJoongd);
                        intent.putExtra("fragAtkorIld", fragAtkorIld);
                        intent.putExtra("fragKorMubiHan", fragKorMubiHan);
                        intent.putExtra("fragKorMubiOver", fragKorMubiOver);
                        intent.putExtra("fragKorMid", fragKorMid);
                        intent.putExtra("fragKorCd", fragKorCd);
                        intent.putExtra("fragKorJd", fragKorJd);
                        intent.putExtra("fragAtkorMubiHanClassic", fragAtkorMubiHanClassic);
                        intent.putExtra("fragAtkorMubiOverClassic", fragAtkorMubiOverClassic);
                        intent.putExtra("fragLiveTv", fragLiveTv);
                        intent.putExtra("fragLiveTvEnter", fragLiveTvEnter);
                        intent.putExtra("fragLiveTvSports", fragLiveTvSports);
                        intent.putExtra("tistoryUrl", tistoryUrl);
                        intent.putExtra("tistoryRndPage", tistoryRndPage+"");
                        intent.putExtra("adsFlag", adsFlag+"");
                        intent.putExtra("stageNum", stageNum+"");
                        intent.putExtra("fragmentDramaJson", fragmentDramaJson);
                        intent.putExtra("fragmentEnterJson", fragmentEnterJson);
                        intent.putExtra("fragmentSisaJson", fragmentSisaJson);
                        intent.putExtra("fragmentJoytvDrama", fragmentJoytvDrama);
                        intent.putExtra("fragmentJoytvEnter", fragmentJoytvEnter);
                        intent.putExtra("fragmentJoytvSisa", fragmentJoytvSisa);
                        intent.putExtra("fragmentEndContentsOtg", fragmentEndContentsOtg);
                        intent.putExtra("fragmentEndContentsTvnara", fragmentEndContentsTvnara);
                    } else if(appStatus.equals("3")){
                        intent.putExtra("closed", closedImgUrl);
                        intent.putExtra("nextAppUrl", nextAppUrl);
                    } else if(appStatus.equals("9")){
                        intent.putExtra("closed", closedImgUrl);
                        intent.putExtra("nextAppUrl", nextAppUrl);
                    } else {
                        intent.putExtra("maintenance", maintenanceImgUrl);
                        intent.putExtra("adsFlag", adsFlag+"");
                    }
                    finish();
                    startActivity(intent);
                }
            }, 3000);
        }
    }

}
