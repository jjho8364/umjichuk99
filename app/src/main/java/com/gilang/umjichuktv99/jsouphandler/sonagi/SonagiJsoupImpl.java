package com.gilang.umjichuktv99.jsouphandler.sonagi;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.gilang.umjichuktv99.activity.list.HoduListActivity;
import com.gilang.umjichuktv99.jsouphandler.SonagiJsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class SonagiJsoupImpl extends AsyncTask<Void, Void, Void>  implements SonagiJsoup {

    private final String TAG = " SonagiJsoupImpl - ";

    @Override
    public void dramaList() {

    }

    @Override
    public String dramaBtnListUrl(String baseUrl, Context context) {
        Document doc = null;
        String linkStr = "";
        try {
            doc = Jsoup.connect(baseUrl).timeout(20000).get();
            Log.d(TAG, "baseUrl : " + baseUrl);
            linkStr = doc.select(".MVthelinkbt").attr("href");
            Log.d(TAG, "linkStr : " + linkStr);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return linkStr;
    }


    @Override
    protected Void doInBackground(Void... voids) {


        return null;
    }
}
