package com.gilang.umjichuktv99.common;

public class CommonMessage {

    public final static String baseUrl = "https://www.viki.com";

    public final static String dramaSearchUrl = "https://www.viki.com/search?q=";
    public final static String searchUrl = "https://www.viki.com/explore?";
    public final static String paramCty = "country=";
    public final static String paramLang = "language=";
    public final static String paramPage = "page=";
    public final static String paramSort = "&sort=latest";
    public final static String paramType = "type=";
    public final static String headerAccLang = "accept-language";
    public final static String paramCurPage = "&page=";
    public final static String paramSeries = "&type=series";

    public final static String headerLang = "ko-KR,ko;q=0.9,id-ID;q=0.8,id;q=0.7,en-US;q=0.6,en;q=0.5";
    public final static String refererFront = "https://www.viki.com/tv/";

    public final static String getStroyUrl1 = "https://api.viki.io/v4/containers/";
    public final static String getStroyUrl2 = ".json?app=100000a";
    public final static String getEpsUrl1 = "https://api.viki.io/v4/containers/";
    public final static String getEpsUrl2 = "/episodes.json?token=undefined&direction=asc&with_upcoming=true&sort=number&blocked=true";
    public final static String getEpsUrl3 = "&per_page=20&app=100000a";
    public final static String getTeaserUrl1 = "https://api.viki.io/v4/containers/";
    public final static String getTeaserUrl2 = "/trailers.json?sort=newest_video&per_page=20&page=1&app=100000a";
    //public final static String getTeaserUrl3 = "&per_page=20&app=100000a";

    public final static String CAULY = "JHRVaBrm";

    public final static long MIN_CLICK_INTERVAL = 300;

    public final static String VAL_CH = "mainland-china";
    public final static String VAL_TA = "taiwan";
    public final static String VAL_KO = "korea";
    public final static String VAL_JP = "japan";
    public final static String VAL_MO = "film";
    public final static String VAL_ENTER = "variety-show";

    public final static String desktopAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36";





}
