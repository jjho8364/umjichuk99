package com.gilang.umjichuktv99.item;

public class ListLand5959Item {
    private String title;
    private String imgUrl;
    private String pageUrl;

    public ListLand5959Item(String title, String imgUrl, String pageUrl) {
        this.title = title;
        this.imgUrl = imgUrl;
        this.pageUrl = pageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }
}
