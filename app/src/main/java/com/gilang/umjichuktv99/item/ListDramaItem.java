package com.gilang.umjichuktv99.item;

public class ListDramaItem {
    private String title;
    private String imgUrl;
    private String epsUrl;
    private String ctn;
    private String idNo;

    public ListDramaItem(String title, String imgUrl, String epsUrl, String ctn, String idNo) {
        this.title = title;
        this.imgUrl = imgUrl;
        this.epsUrl = epsUrl;
        this.ctn = ctn;
        this.idNo = idNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getEpsUrl() {
        return epsUrl;
    }

    public void setEpsUrl(String epsUrl) {
        this.epsUrl = epsUrl;
    }

    public String getCtn() {
        return ctn;
    }

    public void setCtn(String ctn) {
        this.ctn = ctn;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }
}
