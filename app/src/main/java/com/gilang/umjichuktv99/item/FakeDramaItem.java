package com.gilang.umjichuktv99.item;

public class FakeDramaItem {
    String imgUrl;
    String videoUrl;
    String title;

    public FakeDramaItem(String imgUrl, String videoUrl, String title) {
        this.imgUrl = imgUrl;
        this.videoUrl = videoUrl;
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
