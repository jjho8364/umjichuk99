package com.gilang.umjichuktv99.item;

public class GridViewItem {
    private String imgUrl;
    private String title;
    private String yutubUrl;

    public GridViewItem(String imgUrl, String title, String yutubUrl) {
        this.imgUrl = imgUrl;
        this.title = title;
        this.yutubUrl = yutubUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYutubUrl() {
        return yutubUrl;
    }

    public void setYutubUrl(String yutubUrl) {
        this.yutubUrl = yutubUrl;
    }
}
