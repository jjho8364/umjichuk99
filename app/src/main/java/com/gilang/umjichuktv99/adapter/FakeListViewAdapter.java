package com.gilang.umjichuktv99.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gilang.umjichuktv99.R;
import com.gilang.umjichuktv99.item.FakeDramaItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FakeListViewAdapter extends BaseAdapter {
    String TAG = "FakeListViewAdapter";
    Activity context;
    ArrayList<FakeDramaItem> listArr;
    LayoutInflater inf;
    int layout;

    private ArrayList<FakeDramaItem> arraylist;

    static class ViewHolder {
        public ImageView imageView;
        public TextView title;
    }

    public FakeListViewAdapter(Activity context, ArrayList<FakeDramaItem> listArr, int layout) {
        this.context = context;
        this.listArr = listArr;
        this.inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layout = layout;
        this.arraylist = new ArrayList<FakeDramaItem>();
        arraylist.addAll(listArr);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inf.inflate(layout, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageView= (ImageView)rowView.findViewById(R.id.gridview_img);
            viewHolder.title = (TextView)rowView.findViewById(R.id.gridview_title);

            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder)rowView.getTag();
        FakeDramaItem data = listArr.get(position);
        if(data.getImgUrl()==null || data.getImgUrl().equals("") || data.getImgUrl().equals("https:")){
            holder.imageView.setImageResource(R.drawable.noimage);
        } else {
            Picasso.with(context).load(data.getImgUrl()).into(holder.imageView);
        }
        holder.title.setText(data.getTitle());

        return rowView;
    }

    @Override
    public int getCount() {
        return listArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
