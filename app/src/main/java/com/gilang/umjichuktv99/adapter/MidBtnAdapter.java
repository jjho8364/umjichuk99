package com.gilang.umjichuktv99.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.gilang.umjichuktv99.R;

import java.util.ArrayList;

public class MidBtnAdapter extends BaseAdapter {
    LayoutInflater inflater = null;
    private ArrayList<String> ListViewArr = null;
    private int nListCnt = 0;

    public MidBtnAdapter(ArrayList<String> ListViewArr) {
        this.ListViewArr = ListViewArr;
        this.nListCnt = ListViewArr.size();
    }

    static class ViewHolder {
        public Button button;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            final Context context = parent.getContext();

            if (inflater == null) {
                inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }
            convertView = inflater.inflate(R.layout.item_btn_paly, parent, false);

            ViewHolder viewHolder = new ViewHolder();

            viewHolder.button = (Button) convertView.findViewById(R.id.btn_listview_item);

            convertView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder)convertView.getTag();
        holder.button.setText(ListViewArr.get(position));

        return convertView;
    }

    @Override
    public int getCount() {
        return nListCnt;
    }

    @Override
    public Object getItem(int position) {
        return ListViewArr.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
