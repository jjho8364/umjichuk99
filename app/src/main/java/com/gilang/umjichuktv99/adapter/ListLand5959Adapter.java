package com.gilang.umjichuktv99.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gilang.umjichuktv99.R;
import com.gilang.umjichuktv99.item.ListLand5959Item;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListLand5959Adapter extends BaseAdapter {
    String TAG = "ListLand5959Adapter - ";
    Activity context;
    ArrayList<ListLand5959Item> listArr;
    LayoutInflater inf;
    int layout;

    private ArrayList<ListLand5959Item> arraylist;

    static class ViewHolder {
        public ImageView imageView;
        public TextView title;
    }

    public ListLand5959Adapter(Activity context, ArrayList<ListLand5959Item> listArr, int layout) {
        this.context = context;
        this.listArr = listArr;
        this.inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layout = layout;
        this.arraylist = new ArrayList<ListLand5959Item>();
        arraylist.addAll(listArr);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inf.inflate(layout, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageView= (ImageView)rowView.findViewById(R.id.listview_img);
            viewHolder.title = (TextView)rowView.findViewById(R.id.listview_title);

            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder)rowView.getTag();
        ListLand5959Item data = listArr.get(position);
        if(data.getImgUrl()==null || data.getImgUrl().equals("") || data.getImgUrl().equals("https:")){
            holder.imageView.setImageResource(R.drawable.noimage);
        } else {
            Picasso.with(context).load(data.getImgUrl()).into(holder.imageView);
        }
        holder.title.setText(data.getTitle());

        return rowView;
    }

    @Override
    public int getCount() {
        return listArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
