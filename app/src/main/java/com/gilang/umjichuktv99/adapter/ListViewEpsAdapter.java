package com.gilang.umjichuktv99.adapter;

import static android.content.Context.MODE_PRIVATE;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gilang.umjichuktv99.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListViewEpsAdapter extends BaseAdapter {
    String TAG = "ListViewEpsAdapter";
    Activity context;
    int pageNum;
    ArrayList<String> imgArr;
    ArrayList<String> listPageUrlArr;
    LayoutInflater inf;
    int layout;

    private ArrayList<String> arraylist;

    static class ViewHolder {
        public ImageView imageView;
        public TextView title;
    }

    public ListViewEpsAdapter(){

    }

    public ListViewEpsAdapter(Activity context, int pageNum, ArrayList<String> imgArr, ArrayList<String> listPageUrlArr, int layout) {
        this.context = context;
        this.pageNum = pageNum;
        this.imgArr = imgArr;
        this.listPageUrlArr = listPageUrlArr;
        this.inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layout = layout;
        this.arraylist = new ArrayList<String>();
        arraylist.addAll(imgArr);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (convertView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inf.inflate(layout, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView)rowView.findViewById(R.id.item_iv_eps);
            viewHolder.title = (TextView)rowView.findViewById(R.id.item_tv_eps);

            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder)rowView.getTag();
        String imgUrl = imgArr.get(position);
        String viewUrl = listPageUrlArr.get(position);

        SharedPreferences pref= this.context.getSharedPreferences("pref", MODE_PRIVATE); // 선언
        String cekUrl = pref.getString(viewUrl, null);
        if(cekUrl != null && !cekUrl.equals("")){
            holder.title.setTextColor(context.getResources().getColor(R.color.purple_300));
        } else {
            holder.title.setTextColor(context.getResources().getColor(R.color.white));
        }


        if(imgUrl==null || imgUrl.equals("") || !imgUrl.contains("https:")){
            holder.imageView.setImageResource(R.drawable.noimage);
        } else {
            Picasso.with(context).load(imgUrl).into(holder.imageView);
        }

        holder.title.setText("Teaser 영상 " + (pageNum * 20 - 20 + 1 + position));

        return rowView;
    }

    @Override
    public int getCount() {
        return imgArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
